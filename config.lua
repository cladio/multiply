application = {
	content = {
		--  graphicsCompatibility = 1,
		width = 768,
		height = 1024,
		scale = "letterBox",
		fps = 60,
		imageSuffix =
		{
				["@15x"] = 1.5,		-- A good scale for Droid, Nexus One, etc.
				["@2x"] = 2,		-- A good scale for iPhone 4 and iPad
			
		},
	}
}