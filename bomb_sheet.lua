--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:b91ac76066b8702656601e801c011ef2:cb62386710f50e0abf6fa84c2e6dc0fa:be2ce8b85423a598c6a1e195fbf761c1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- bomb_00@2x
            x=202,
            y=116,
            width=97,
            height=41,

            sourceX = 26,
            sourceY = 56,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_01@2x
            x=103,
            y=134,
            width=97,
            height=44,

            sourceX = 26,
            sourceY = 53,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_02@2x
            x=358,
            y=2,
            width=97,
            height=38,

            sourceX = 26,
            sourceY = 56,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_03@2x
            x=301,
            y=140,
            width=97,
            height=38,

            sourceX = 26,
            sourceY = 56,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_04@2x
            x=301,
            y=100,
            width=97,
            height=38,

            sourceX = 26,
            sourceY = 56,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_05@2x
            x=400,
            y=156,
            width=97,
            height=36,

            sourceX = 26,
            sourceY = 57,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_06@2x
            x=400,
            y=118,
            width=99,
            height=36,

            sourceX = 25,
            sourceY = 57,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_07@2x
            x=400,
            y=80,
            width=99,
            height=36,

            sourceX = 25,
            sourceY = 57,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_08@2x
            x=358,
            y=42,
            width=101,
            height=36,

            sourceX = 24,
            sourceY = 57,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_09@2x
            x=2,
            y=134,
            width=99,
            height=45,

            sourceX = 25,
            sourceY = 53,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_10@2x
            x=254,
            y=2,
            width=102,
            height=96,

            sourceX = 24,
            sourceY = 27,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_11@2x
            x=136,
            y=2,
            width=116,
            height=112,

            sourceX = 17,
            sourceY = 19,
            sourceWidth = 150,
            sourceHeight = 150
        },
        {
            -- bomb_12@2x
            x=2,
            y=2,
            width=132,
            height=130,

            sourceX = 8,
            sourceY = 12,
            sourceWidth = 150,
            sourceHeight = 150
        },
    },
    
    sheetContentWidth = 501,
    sheetContentHeight = 194
}

SheetInfo.frameIndex =
{

    ["bomb_00@2x"] = 1,
    ["bomb_01@2x"] = 2,
    ["bomb_02@2x"] = 3,
    ["bomb_03@2x"] = 4,
    ["bomb_04@2x"] = 5,
    ["bomb_05@2x"] = 6,
    ["bomb_06@2x"] = 7,
    ["bomb_07@2x"] = 8,
    ["bomb_08@2x"] = 9,
    ["bomb_09@2x"] = 10,
    ["bomb_10@2x"] = 11,
    ["bomb_11@2x"] = 12,
    ["bomb_12@2x"] = 13,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
