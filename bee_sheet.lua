--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:04cd1eadb4cb1b93a6bbe9dce9d4206d:c27c45e74e37c472c96c150077946e06:e920b8fabf63e2092e3337a124ad7ec9$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- 1-1
            x=382,
            y=2,
            width=60,
            height=77,

            sourceX = 20,
            sourceY = 9,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1-2
            x=320,
            y=83,
            width=61,
            height=77,

            sourceX = 20,
            sourceY = 9,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-1
            x=323,
            y=162,
            width=70,
            height=74,

            sourceX = 15,
            sourceY = 13,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-10
            x=293,
            y=264,
            width=74,
            height=76,

            sourceX = 12,
            sourceY = 12,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-2
            x=316,
            y=2,
            width=64,
            height=79,

            sourceX = 18,
            sourceY = 12,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-3
            x=256,
            y=89,
            width=62,
            height=80,

            sourceX = 19,
            sourceY = 10,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-4
            x=230,
            y=284,
            width=61,
            height=82,

            sourceX = 19,
            sourceY = 9,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-5
            x=157,
            y=375,
            width=61,
            height=85,

            sourceX = 19,
            sourceY = 8,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-6
            x=91,
            y=375,
            width=64,
            height=85,

            sourceX = 18,
            sourceY = 7,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-7
            x=161,
            y=284,
            width=67,
            height=84,

            sourceX = 17,
            sourceY = 8,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-8
            x=246,
            y=2,
            width=68,
            height=85,

            sourceX = 15,
            sourceY = 7,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 1.5-9
            x=252,
            y=181,
            width=69,
            height=81,

            sourceX = 14,
            sourceY = 9,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 2-1
            x=166,
            y=2,
            width=78,
            height=89,

            sourceX = 11,
            sourceY = 9,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 2-2
            x=80,
            y=196,
            width=78,
            height=88,

            sourceX = 11,
            sourceY = 6,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 2-3
            x=85,
            y=2,
            width=79,
            height=89,

            sourceX = 10,
            sourceY = 2,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 2-4
            x=80,
            y=286,
            width=79,
            height=87,

            sourceX = 10,
            sourceY = 7,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-1
            x=2,
            y=304,
            width=76,
            height=98,

            sourceX = 12,
            sourceY = 1,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-2
            x=2,
            y=204,
            width=76,
            height=98,

            sourceX = 12,
            sourceY = 1,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-3
            x=2,
            y=104,
            width=76,
            height=98,

            sourceX = 12,
            sourceY = 1,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-4
            x=2,
            y=2,
            width=81,
            height=100,

            sourceX = 9,
            sourceY = 0,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-5
            x=2,
            y=404,
            width=87,
            height=96,

            sourceX = 6,
            sourceY = 2,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-6
            x=80,
            y=104,
            width=87,
            height=90,

            sourceX = 3,
            sourceY = 3,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-7
            x=160,
            y=196,
            width=90,
            height=86,

            sourceX = 5,
            sourceY = 7,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-8
            x=220,
            y=370,
            width=89,
            height=83,

            sourceX = 5,
            sourceY = 8,
            sourceWidth = 100,
            sourceHeight = 100
        },
        {
            -- 3-9
            x=169,
            y=93,
            width=85,
            height=86,

            sourceX = 6,
            sourceY = 6,
            sourceWidth = 100,
            sourceHeight = 100
        },
    },
    
    sheetContentWidth = 512,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["1-1"] = 1,
    ["1-2"] = 2,
    ["1.5-1"] = 3,
    ["1.5-10"] = 4,
    ["1.5-2"] = 5,
    ["1.5-3"] = 6,
    ["1.5-4"] = 7,
    ["1.5-5"] = 8,
    ["1.5-6"] = 9,
    ["1.5-7"] = 10,
    ["1.5-8"] = 11,
    ["1.5-9"] = 12,
    ["2-1"] = 13,
    ["2-2"] = 14,
    ["2-3"] = 15,
    ["2-4"] = 16,
    ["3-1"] = 17,
    ["3-2"] = 18,
    ["3-3"] = 19,
    ["3-4"] = 20,
    ["3-5"] = 21,
    ["3-6"] = 22,
    ["3-7"] = 23,
    ["3-8"] = 24,
    ["3-9"] = 25,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
