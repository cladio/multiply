--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:b777008d4f1b2cce60c9d35bef06c774:7eda66813e44839c70dbb0c2bd9b484a:98361c2b2067641a4e7452b2defaf41d$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- attacted2
            x=192,
            y=775,
            width=188,
            height=138,

            sourceX = 6,
            sourceY = 31,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- attacted3
            x=2,
            y=510,
            width=192,
            height=124,

            sourceX = 4,
            sourceY = 38,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- attacted4
            x=2,
            y=257,
            width=192,
            height=125,

            sourceX = 4,
            sourceY = 37,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- attacted5
            x=202,
            y=2,
            width=198,
            height=125,

            sourceX = 1,
            sourceY = 37,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- attacted6
            x=196,
            y=256,
            width=192,
            height=125,

            sourceX = 4,
            sourceY = 37,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- attacted7
            x=196,
            y=509,
            width=192,
            height=124,

            sourceX = 4,
            sourceY = 38,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- attacted8
            x=2,
            y=776,
            width=188,
            height=138,

            sourceX = 6,
            sourceY = 31,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- 거미1
            x=2,
            y=636,
            width=188,
            height=138,

            sourceX = 6,
            sourceY = 31,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- 거미2
            x=2,
            y=384,
            width=192,
            height=124,

            sourceX = 4,
            sourceY = 38,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- 거미3
            x=2,
            y=130,
            width=192,
            height=125,

            sourceX = 4,
            sourceY = 38,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- 거미4
            x=2,
            y=2,
            width=198,
            height=126,

            sourceX = 1,
            sourceY = 37,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- 거미5
            x=202,
            y=129,
            width=192,
            height=125,

            sourceX = 4,
            sourceY = 38,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- 거미6
            x=196,
            y=383,
            width=192,
            height=124,

            sourceX = 4,
            sourceY = 38,
            sourceWidth = 200,
            sourceHeight = 200
        },
        {
            -- 거미7
            x=196,
            y=635,
            width=188,
            height=138,

            sourceX = 6,
            sourceY = 31,
            sourceWidth = 200,
            sourceHeight = 200
        },
    },
    
    sheetContentWidth = 512,
    sheetContentHeight = 1024
}

SheetInfo.frameIndex =
{

    ["attacted2"] = 1,
    ["attacted3"] = 2,
    ["attacted4"] = 3,
    ["attacted5"] = 4,
    ["attacted6"] = 5,
    ["attacted7"] = 6,
    ["attacted8"] = 7,
    ["거미1"] = 8,
    ["거미2"] = 9,
    ["거미3"] = 10,
    ["거미4"] = 11,
    ["거미5"] = 12,
    ["거미6"] = 13,
    ["거미7"] = 14,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
