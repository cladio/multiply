module(..., package.seeall)

local ads = require "ads"

---------------------- checking In App Purchase --------------------------------


function checkPurchaseAds ()
	
	DEBUG("user.checkPurchaseAds()");
	
	local iapOldFileName = "iap.json"
	local iapNewFileName = ".iapHiddenFile.json"


	local f = io.open( system.pathForFile( iapOldFileName, system.DocumentsDirectory ), "r" )
	if f ~= nil then
		io.close(f)
		local destDir = system.DocumentsDirectory  -- where the file is stored
		local results, reason = os.rename( system.pathForFile( iapOldFileName, destDir  ),
        system.pathForFile( iapNewFileName, destDir  ) )
	end

	local storedData = dataIO.readJsonFile(iapNewFileName);
	
	if not storedData then
		storedData = { purchased = false};
		dataIO.writeJsonFile(storedData, iapNewFileName);
	end
	
	DEBUG("purchased? = ", storedData.purchased)
	
	return storedData.purchased
	
end

function savePurchaseAds()

	local iapFileName = ".iapHiddenFile.json"
	local storedData = { purchased = true};
	dataIO.writeJsonFile(storedData, iapFileName);

end

function buyRemoveAds()
	
	if checkPurchaseAds() == true then
		native.showAlert( trans("광고제거"), trans("이미구매함"), {"OK"})
		return;
	end
	
	-- Handler that gets notified when the alert closes
	local function onComplete( event )
		if "clicked" == event.action then
			local i = event.index
			if 1 == i then
				iap.restore();
			elseif 2 == i then
				iap.loadAndPurchaseRemoveAds();
			end
		end
	end
	
	-- Show alert with two buttons
	local alert = native.showAlert( trans("광고제거"), trans("산적있나요?"), { "Yes", "No" }, onComplete )

	
	
	
end


----------------------------- Ads -------------------------------------

-- init with Inneractive, and if fail to receive ads, then init with inmobi

adType = "not initialized"
local adListener -- function forward declaration
local adHide = false;
local adInitFlag = false;

function showAd()
	
	if adsRemovedFlag then
		return;
	end

	local adSpace = 0
	if iOSversion:sub(1,1) == "6" then
		adSpace = -20
		print("Ads(admob, iads) : added space -25 to iOS6")
	end
	
	removeAdsBanner.isVisible = true;
	transition.to (tabBar, { time=200, y = screenH - 75} )

	if isAndroid then
		if(adType == "inmobi") then
			ads.show( "banner320x48", { x=0, y=screenH - 50 , interval = 30, testMode=false } )
		elseif (adType == "inneractive") then
			ads.show ( "banner", {x=0, y=screenH - 50, interval = 30, testMode=false} );	
		elseif (adType == "admob") then
			ads.show ( "banner", {x=0, y=screenH  - 50 } );	
		end
	else
		if(adType == "inmobi") then
			ads.show( "banner320x48", { x=0, y=screenH - 50 , interval = 30, testMode=false } )
		elseif (adType == "inneractive") then
			ads.show ( "banner", {x=0, y=screenH - 50, interval = 30, testMode=false} );	
		elseif (adType == "admob") then
			ads.show ( "banner", {x=0, y=screenH + adSpace} );	
		elseif (adType == "iads") then
			ads.show ( "banner", {x=0, y=screenH + adSpace} );
		end
	end


	adHide = false;
	print("showAd = ", adType)
end

function hideAd ()	
	
	if adsRemovedFlag then
		return;
	end
	
	-- just move ad below the screen.
	
	transition.to (tabBar, { time=200, y = screenH - 25} )
	removeAdsBanner.isVisible = false;

	ads.hide();
	adHide = true;
	DEBUG("hideAd()!");
	
end

-- this function is only called when in-app-purchase 'buy' transaction completed successfully --

function removeAds()
    
    _G.adsRemovedFlag = true;
    
	ads.hide();
	adHide = true
	tabBar.y = screenH -25;
	if removeAdsBanner then
		removeAdsBanner.isVisible = false;
	end;	
        
end


function initAds ()

	if adInitFlag == true then
		switchAds();
		return;
	end
	
	adInitFlag = true;

	if(isAndroid) then 
		ads.init( "inmobi", "4028cbff39009b24013956e6dbbe0703", adListener ) 
		ads.init ("admob", "a15036bd5b43e91", adListener)
	else
		ads.init ("admob", "b50eab01f83c41c0", adListener)
		ads.init( "inmobi", "4028cbff379738bf01383728b100238d", adListener )
		ads.init ("iads", "548649499", adListener)
	end

	ads:setCurrentProvider(adType)


end

local function switchAds()
	ads.hide()
	ads:setCurrentProvider(adType)
	showAd();
end

function refreshAds()
	if adHide == false then
		ads.hide()
		showAd();
	end
end

function adListener(event)
	print("adListener Called! event.isError = ", event.isError, adType);
	
	if event.isError then
		DEBUG("Failed: ", adType)
		if adType == "inmobi" then
			adType = "admob"
		elseif adType == "admob" then
			if isAndroid then
				adType = "inmobi"
			else
				adType = "iads"
			end
		elseif adType == "iads" then
			adType = "inmobi"
		end
		print("Changed to :", adType)
		timer.performWithDelay( 4000, switchAds )
		
	else
		print("Ad Successful!", adType)
	end

end

function initFromUrl()

    local function initRequestListener(event)
    
        if event.isError then
            adType = "inmobi"

            DEBUG("user.initFromUrl: network request error");
        else
 	--[[        	local test = {
						   android = {
						      US = {"inmobbi", "inmobi", "admob"},
						      KR = {"A", "B"}
						  	}
						  }
			print(json.encode(test))
	]]
			local config = json.decode(event.response)
			local country = system.getPreference("locale", "country")

			local randomInt = 0;
			if isAndroid == false then
				local uniqueString = system.getInfo("iosAdvertisingIdentifier")
				local uniqueSum = 0;

				if uniqueString then
				    for i=1, string.len(uniqueString) do
				        uniqueSum = uniqueSum + string.byte(uniqueString, i)
				    end
				end

				local today = os.date("*t")
				uniqueSum = uniqueSum + today.yday

				randomInt = uniqueSum % 10

			end

			if not config then
				adType = "inmobi"
			elseif isAndroid then
				adType = config.android[country][randomInt] or config.android.default[randomInt] ;
			else 
				adType = config.iOS[country][randomInt] or config.iOS.default[randomInt] ;
			end
			
			if adType ~= "inmobi" and adType ~= "inneractive" and adType ~= "admob" and adType ~= "iads" then
				adType = "inmobi"
			end
	
			print("ad init from URL : country code, adType, randomInt", country, adType, randomInt);	
		end

		initAds()
		if not adHide then
			ads.hide();
			showAd();
		end
    end
    
    DEBUG("request ad_init3 to network")
    network.request("http://www.creaplay.net/nursingtimer/ad_init3.config?" .. os.time(), "GET", initRequestListener)

end




function newRectButton(params)


	local obj = display.newGroup();

	obj.rect = display.newRect(obj, 0, 0, params.width or 100, params.height or 100)
	obj.rect.strokeWidth = params.strokeWidth or 0;
	obj.rect:setStrokeColor(params.strokeColor[1] or 1, params.strokeColor[2] or 1, params.strokeColor[3] or 1)
	obj.rect:setFillColor(params.defaultColor[1] or 1, params.defaultColor[2] or 1, params.defaultColor[3] or 1);

	if params.label then
		obj.label = display.newText(obj, "", 0, 0, params.font or native.systemFont, params.fontSize or 10)
		obj.label.text = params.label or "";
		obj.label:setFillColor(params.fontDefaultColor[1] or 1, params.fontDefaultColor[2] or 1, params.fontDefaultColor[3] or 1)
--		obj.label:setReferencePoint(display.CenterReferencePoint);
		obj.label.x = obj.rect.x + (params.labelOffsetX or 0);
		obj.label.y = obj.rect.y + (params.labelOffsetY or 0);
	end

	if params.defaultImage then
		obj.image = display.newImageRect(obj, params.defaultImage.filename, params.defaultImage.width, params.defaultImage.height)
		obj.image.x = obj.rect.x + (params.defaultImage.offsetX or 0)
		obj.image.y = obj.rect.y + (params.defaultImage.offsetY or 0)
		obj.image:setFillColor(params.defaultImage.color[1] or 1, params.defaultImage.color[2] or 1, params.defaultImage.color[3] or 1)
	end

	if params.defaultImage2 then
		obj.image2 = display.newImageRect(obj, params.defaultImage2.filename, params.defaultImage2.width, params.defaultImage2.height)
		obj.image2.x = obj.rect.x + (params.defaultImage2.offsetX or 0)
		obj.image2.y = obj.rect.y + (params.defaultImage2.offsetY or 0)
		obj.image2:setFillColor(params.defaultImage2.color[1] or 1, params.defaultImage2.color[2] or 1, params.defaultImage2.color[3] or 1)
	end


	if params.overImage then
		obj.imageOver = display.newImageRect(obj, params.overImage.filename, params.overImage.width, params.overImage.height)
		obj.imageOver.x = obj.rect.x + (params.overImage.offsetX or 0)
		obj.imageOver.y = obj.rect.y + (params.overImage.offsetY or 0)
		obj.imageOver:setFillColor(params.overImage.color[1] or 1, params.overImage.color[2] or 1, params.overImage.color[3] or 1)
		obj.imageOver.isVisible = false;
	end


	obj.id = params.id or "id"

	local function onTouch( event)
		if event.phase == "began" then
			obj.rect:setFillColor(params.defaultColor[1]*0.7, params.defaultColor[2]*0.7, params.defaultColor[3]*0.7);
			display.getCurrentStage():setFocus( event.target)
			obj.isFocus = true
		elseif obj.isFocus then	
			local bounds = obj.stageBounds
			local x,y = event.x,event.y
			local isWithinBounds = 
				bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y
			
			if event.phase == "ended" then
				obj.rect:setFillColor(params.defaultColor[1], params.defaultColor[2], params.defaultColor[3]);
				if isWithinBounds then
					params.onEvent(event);
				end
				display.getCurrentStage():setFocus(  nil )
				obj.isFocus = false
			end
		end
		return true;
	end

	obj.setLabel = function (label, color)
		obj.label.text = label;
		if color then
			obj.label:setFillColor(color[1] or 1, color[2] or 1, color[2] or 1)
		end
	end

	obj.setDefaultImageColor = function (color)
		obj.image:setFillColor(color[1] or 1, color[2] or 1, color[2] or 1)
	end

	obj.setDefaultImageColor2 = function (color)
		obj.image2:setFillColor(color[1] or 1, color[2] or 1, color[2] or 1)
	end

	obj.setOverImageColor = function (color)
		obj.imageOver:setFillColor(color[1] or 1, color[2] or 1, color[2] or 1)
	end

	obj.setDefaultImage = function()
		obj.image.isVisible = true;
		obj.imageOver.isVisible = false;
	end

	obj.setOverImage = function()
		obj.image.isVisible = false;
		obj.imageOver.isVisible = true;
	end

	

	obj.setColor = function(colorType)
		if colorType == "default" then
			obj.rect:setFillColor(params.defaultColor[1] or 1, params.defaultColor[2] or 1, params.defaultColor[3] or 1);
		elseif colorType == "over" then
			obj.rect:setFillColor(params.overColor[1] or 1, params.overColor[2] or 1, params.overColor[3] or 1);
		end
	end

	obj.setActive = function()
		obj:removeEventListener("touch", onTouch)
		obj:addEventListener("touch", onTouch)
	end

	obj.setInactive = function()
		obj:removeEventListener("touch", onTouch)
	end

	obj:addEventListener("touch", onTouch)



	return obj;
end



function newRoundButton(params)

	--[[ Usage
	test = newRoundButton({
				{	id = "id",
				radius = , 
				height = , 
				strokeColor = ,
				strokeWidth = ,
				defaultColor = ,
				overcolor = ,

				label = ,
				labelOffsetX = ,
				labelOffsetY = , 
				font = ,
				fontSize,
				fontDefaultColor = ,
				fontOverColor = ,

				defaultImage = {
					filename = ,
					width = ,
					height = ,
					offsetX = ,
					offsetY =, 
					color =
				},
				onEvent = 
			})

	--]]
	local obj = display.newGroup();


--	obj.circle = display.newCircle(obj, 0, 0, params.radius-1 or 50)
--	obj.circle.strokeWidth = 0;
	obj.circle = display.newImageRect(obj, "images/circle_big_fill.png", params.radius * 2 or 50, params.radius * 2 or 50)
	obj.circle:setFillColor(params.defaultColor[1] or 1, params.defaultColor[2] or 1, params.defaultColor[3] or 1, params.defaultColor[4] or 1);
	
--	obj.stroke = display.newImageRect(obj, "images/circle_big_stroke.png", params.radius * 2 or 50, params.radius * 2 or 50)
--	obj.stroke:setFillColor(params.strokeColor[1] or 1, params.strokeColor[2] or 1, params.strokeColor[3] or 1, params.strokeColor[4] or 1)
--	if params.strokeWidth == 0 then obj.stroke.isVisible = false end
	
	if params.label then
		obj.label = display.newText(obj, "", 0, 0, params.font or native.systemFont, params.fontSize or 10)
		obj.label.text = params.label or "";
		obj.label:setFillColor(params.fontDefaultColor[1] or 1, params.fontDefaultColor[2] or 1, params.fontDefaultColor[3] or 1)
--		obj.label:setReferencePoint(display.CenterReferencePoint);
		obj.label.x = obj.circle.x + (params.labelOffsetX or 0);
		obj.label.y = obj.circle.y + (params.labelOffsetY or 0);
	end

	if params.defaultImage then
		obj.image = display.newImageRect(obj, params.defaultImage.filename, params.defaultImage.width, params.defaultImage.height)
		obj.image.x = obj.circle.x + (params.defaultImage.offsetX or 0)
		obj.image.y = obj.circle.y + (params.defaultImage.offsetY or 0)
		obj.image:setFillColor(params.defaultImage.color[1] or 1, params.defaultImage.color[2] or 1, params.defaultImage.color[3] or 1)
	end

	if params.overImage then
		obj.imageOver = display.newImageRect(obj, params.overImage.filename, params.overImage.width, params.overImage.height)
		obj.imageOver.x = obj.circle.x + (params.overImage.offsetX or 0)
		obj.imageOver.y = obj.circle.y + (params.overImage.offsetY or 0)
		obj.imageOver:setFillColor(params.overImage.color[1] or 1, params.overImage.color[2] or 1, params.overImage.color[3] or 1)
		obj.imageOver.isVisible = false;
	end

	obj.id = params.id or "id"

	local function onTouch( event)
		if event.phase == "began" then
			obj.circle:setFillColor(params.defaultColor[1]*0.7, params.defaultColor[2]*0.7, params.defaultColor[3]*0.7, params.defaultColor[4] or 1);
			display.getCurrentStage():setFocus( event.target)
			obj.isFocus = true
		elseif obj.isFocus then	
			local bounds = obj.stageBounds
			local x,y = event.x,event.y
			local isWithinBounds = 
				bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y
			
			if event.phase == "ended" then
				obj.circle:setFillColor(params.defaultColor[1], params.defaultColor[2], params.defaultColor[3], params.defaultColor[4] or 1);
				if isWithinBounds then
					params.onEvent(event);
				end
				display.getCurrentStage():setFocus(  nil )
				obj.isFocus = false
			end
		end
		return true;
	end

	obj.setLabel = function (label, color)
		obj.label.text = label;
		if color then
			obj.label:setFillColor(color[1] or 1, color[2] or 1, color[2] or 1)
		end
	end

	obj.setDefaultImageColor = function (color)
		obj.image:setFillColor(color[1] or 1, color[2] or 1, color[2] or 1)
	end

	obj.setOverImageColor = function (color)
		obj.imageOver:setFillColor(color[1] or 1, color[2] or 1, color[2] or 1)
	end

	obj.setDefaultImage = function()
		obj.image.isVisible = true;
		obj.imageOver.isVisible = false;
	end

	obj.setOverImage = function()
		obj.image.isVisible = false;
		obj.imageOver.isVisible = true;
	end

	

	obj.setColor = function(colorType)
		if colorType == "default" then
			obj.circle:setFillColor(params.defaultColor[1] or 1, params.defaultColor[2] or 1, params.defaultColor[3] or 1, params.defaultColor[4] or 1);
--			obj.stroke:setFillColor(params.defaultColor[1] or 1, params.defaultColor[2] or 1, params.defaultColor[3] or 1, params.defaultColor[4] or 1)
		elseif colorType == "over" then
			obj.circle:setFillColor(params.overColor[1] or 1, params.overColor[2] or 1, params.overColor[3] or 1, params.overColor[4] or 1);
--			obj.stroke:setFillColor(params.overColor[1] or 1, params.overColor[2] or 1, params.overColor[3] or 1, params.overColor[4] or 1)
		end
	end

	obj.setActive = function()
		obj:removeEventListener("touch", onTouch)
		obj:addEventListener("touch", onTouch)
	end

	obj.setInactive = function()
		obj:removeEventListener("touch", onTouch)
	end

	obj:addEventListener("touch", onTouch)

	return obj;
end


function makeDummyALOT ()
	local feedingInfo = {};
		feedingInfo.changeTime = 615;
		feedingInfo.feedingTime = 1230;
		feedingInfo.feedingType = "L"
		feedingInfo.bottleAmount = 120;
		feedingInfo.bottleUnit = "ml"
		feedingInfo.diaperType = "wet"
	for i=1,500,1 do
		feedingInfo.startTime = os.time() - i*math.random(5000,7000)	
		feedingInfo.feedingTime = math.random(100, 3000)
		feedingInfo.changeTime = math.random(0, feedingInfo.feedingTime)
		local temp = math.random(1,6)
		if temp%6 == 0 then 
			feedingInfo.feedingType = "L"
			feedingInfo.changeTime = 0
		elseif temp%6 == 1 then 
			feedingInfo.feedingType = "R"
			feedingInfo.changeTime = 0
		elseif temp%6 == 2 then feedingInfo.feedingType = "LR"   
		elseif temp%6 == 3 then feedingInfo.feedingType = "RL"
		elseif temp%6 == 4 then feedingInfo.feedingType = "BF"
		elseif temp%6 == 5 then feedingInfo.feedingType = "BP"
		end
		feedingInfo.bottleAmount = math.random(1,30)*10
		accessDB.insert(feedingInfo, "feeding");
	end

	for i=1,500,1 do
		local temp = math.random(1,2)
		if temp%2 == 0 then feedingInfo.feedingType = "PL" 
		else feedingInfo.feedingType = "PR"
		end
		feedingInfo.startTime = os.time() - i*math.random(5000,7000)
		feedingInfo.feedingTime = math.random(100, 3000)
		feedingInfo.bottleAmount = math.random(1,30)*10
		accessDB.insert(feedingInfo, "feeding");
	end

	for i=1,500,1 do
		feedingInfo.startTime = os.time() - i*math.random(5000,7000)
		local temp = math.random(1,3)
		if temp%3 == 0 then feedingInfo.feedingType = "wet"
		elseif temp%3 == 1 then feedingInfo.feedingType = "dung"
		elseif temp%3 == 2 then feedingInfo.feedingType = "both"
		end
		accessDB.insert(feedingInfo, "diaper");
	end

	for i=1,500,1 do
		feedingInfo.startTime = os.time() - i*math.random(5000,7000)	
		feedingInfo.endTime = feedingInfo.startTime + math.random(1000, 30000)	
		accessDB.insert(feedingInfo, "sleep");
	end

end



function checkAMPM ()
	
	local fileName = "AMPM.json"

	local storedData = dataIO.readJsonFile(fileName);
	
	if not storedData then
		storedData = { AMPM = true};
		dataIO.writeJsonFile(storedData, fileName);
	end

	return storedData.AMPM
	
end

function saveAMPM(flag)

	local fileName = "AMPM.json"
	local dataToSave= { AMPM = flag};
	dataIO.writeJsonFile(dataToSave, fileName);

end


function tellTimeInHourMin(timeInSec)
	local time = os.date("*t",timeInSec)
	if checkAMPM() then
		local AMPM = "AM"
		if time.hour >= 12 then
			AMPM = "PM"
			if time.hour ~= 12 then
				time.hour = time.hour - 12
			end
		end

		return string.format("%s %2d:%02d", AMPM ,time.hour ,time.min )
	else
		return string.format("%2d:%02d",time.hour ,time.min )
	end
end


function checkDayMode ()
	
	local fileName = ".dayMode.json"

	local storedData = dataIO.readJsonFile(fileName);
	
	if not storedData then
		storedData = { dayFlag = true};
		dataIO.writeJsonFile(storedData, fileName);
	end
	--print("checkDayMode:", storedData.dayFlag)
	return storedData.dayFlag
	
end

function saveDayMode(flag)

	local fileName = ".dayMode.json"
	local dataToSave= { dayFlag = flag};
	dataIO.writeJsonFile(dataToSave, fileName);

end


