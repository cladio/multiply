module(..., package.seeall)

function new()

	local obj = display.newGroup();


	local sheetInfo = require("spider_sheet")
	local myImageSheet = graphics.newImageSheet( "images/spider_sheet.png", sheetInfo:getSheet() )

	local sequenceData =
	{	
		{
		    name="attacked",
		    frames= { 1, 2, 3, 4, 5, 6, 7},
		    time= 1000,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "bounce"    -- Optional. Values include: "forward","bounce"
		},

		{
		    name="normal",
		    frames= { 8,9,10,11,12,13,14},
		    time= 1000,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "bounce"    -- Optional. Values include: "forward","bounce"
		},
	}

	obj.spider = display.newSprite(obj, myImageSheet, sequenceData )


	obj.move = function(x1, y1)
		obj.spider:setSequence("normal")
		obj.spider:play();
		transition.to(obj, {time = 3000, x=x1, y= y2})
	end

	obj.die = function (target)

		transition.to(obj, {time = 100, rotation = 180})

		timer.performWithDelay(120, function () 
										obj:rotate(180) 
										obj.spider:setSequence("attacked")
										obj.spider:play()
										transition.to(obj, {time = 1700, y = screenH*1.6, transition = easing.inOutBack}) end)

	end


	return obj;

end


