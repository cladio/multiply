module(..., package.seeall)

function new(number, colorTable)

	local obj = display.newGroup();
	
	local sheetInfo = require("bomb_sheet")
	local myImageSheet = graphics.newImageSheet( "images/bomb_sheet.png", sheetInfo:getSheet() )

	local sequenceData =
	{	
		{
		    name="normal",
		    frames= { 2},
		    time= 2000,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "forward"    -- Optional. Values include: "forward","bounce"
		},
		{
		    name="fireup",
		    frames= { 1, 3, 4, 5, 6, 7, 8},
		    time= 1000,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 1,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "forward"    -- Optional. Values include: "forward","bounce"
		},
		{
		    name="explosion",
		    frames= { 9, 10, 11, 12, 13},
		    time= 300,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 1,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "forward"    -- Optional. Values include: "forward","bounce"
		},

	}

	obj.bomb = display.newSprite(obj, myImageSheet, sequenceData )
	obj.bomb:setSequence("normal");
	obj.bomb:play();
	obj.bomb:setFillColor(0.6)

	obj.text = display.newText(obj, number, obj.bomb.x-4, obj.bomb.y+6 , "Futura-Medium", 13);
	obj.text:setFillColor(1)


	obj.setText = function(textArg)
		obj.text.text = textArg;
	end

	obj.setColor = function(tbl)
		obj.bomb:setFillColor(tbl[1],tbl[2], tbl[3])
	end

	obj.setActive = function ()
		obj.bomb:setFillColor(colorTable[1],colorTable[2], colorTable[3])
	end

	obj.setInactive = function ()
		obj.bomb:setFillColor(0.6)
	end

	obj.setStatusNormal = function ()
		obj.bomb:setSequence("normal");
		obj.bomb:play();
	end

	obj.explode = function ()
		obj.bomb:setSequence("fireup");
		obj.bomb:play();
		timer.performWithDelay(1100, function () 
											obj.bomb:setSequence("explosion");
											obj.bomb:play();

										end)
		timer.performWithDelay(1450, function () obj.isVisible = false end)
	end


	return obj;

end
