module(..., package.seeall)

function new(number, colorTable)

	local obj = display.newGroup();
	
	obj.circle = display.newRoundedRect(obj, 0, 0, 150, 70, 35)

	obj.circle:setFillColor(0.6)

	obj.text = display.newText(obj, number, obj.circle.x, obj.circle.y , "Futura-Medium", 24);
	obj.text:setFillColor(1)


	obj.setText = function(text)
		obj.text.text = textArg;
	end

	obj.setColor = function(tbl)
		obj.circle:setFillColor(tbl[1],tbl[2], tbl[3])
	end

	obj.setActive = function ()
		obj.circle:setFillColor(colorTable[1],colorTable[2], colorTable[3])
	end

	obj.setInactive = function ()
		obj.circle:setFillColor(0.6)
	end

	return obj;

end


