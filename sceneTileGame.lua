local storyboard = require( "storyboard" )
local block = require("block_bee")
local answerBomb = require("answerBomb")
local monster = require("monster")
local widget = require("widget")
local spider = require("spider")


local printEmpty = function()	return; end
local DEBUG = printEmpty; 



local scene = storyboard.newScene()

----------------------------------------------------------------------------------
-- 
--      NOTE:
--      
--      Code outside of listener functions (below) will only be executed once,
--      unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

local G_levelText;
local G_gameBlocks;
local G_currentLevel
local G_levelData;
local G_equation;
local G_stepByStepLevelData

local G_randomSize;
local G_answerNumber;

local G_defaultBaloonColor = {100, 100, 100}
local G_playStep;
local G_monster


local function makeRandomGameOnLevel (width, height, numberOfAnswers, level)
	
	local tbl = {};
	for y=1, 40 do
		tbl[y] = {}
		for x=1, 40 do
			tbl[y][x] = 0
		end
	end

	local function printBlocks()
		-- for checking
		for i=1, #tbl do
			local str= ""
			for j=1, #tbl[i] do
				str = str .. tbl[i][j]
			end
			print(str);
		end
	end

	local function makeBlock(level)

		local block = {}
		if level then 
			block.width = level
		else
			block.width = math.random(2,9)
		end
		block.height = math.random(2,9)

		
		if math.random(1,2) == 2 then
		block.width, block.height = block.height , block.width
		end
		

		return block

	end

	local function makeBlockLowerThanLevel(level)

		-- 여기 만들기!
		local block = {}
		block.width = math.random(2,level - 1)
		block.height = math.random(2,level - 1)

		
		if math.random(1,2) == 2 then
		block.width, block.height = block.height , block.width
		end
		
		--print("small block made: ", block.height, block.width)
		return block

	end

	
	local function findPlaceOnTable(argBlock, targetTable)

		local returnBlock = { x = 0, y = 0, number = 0}

		-- 시작점은 1,1 부터 해서 argBlock을 모든 x,y를 시작점으로 배치해서 기존 블럭들과 가장 많이 맞닿는 점을 찾을것.
		for y=1, 40-argBlock.height do
			for x=1, 40-argBlock.width do

				local contact = 0;

				(function ()
					-- x, y에서 시작하는 블럭의 각 칸별로, 옆에 targetTable의 '1'인 칸이 있으면 contact를 증가시킴. targetTable과 겹치면 바로 return.
					for blockY=y, y+argBlock.height-1 do
						for blockX = x, x+argBlock.width-1 do
							if targetTable[blockY][blockX] ~= 0 then return; end
							if (blockY-1 >= 1) and targetTable[blockY-1][blockX] ~= 0 then	contact = contact + 1; end
							if (blockX-1 >= 1) and targetTable[blockY][blockX-1] ~= 0 then	contact = contact + 1; end
							if (blockY+1 <= 40) and targetTable[blockY+1][blockX] ~= 0 then	contact = contact + 1; end
							if (blockX+1 <= 40) and targetTable[blockY][blockX+1] ~= 0 then	contact = contact + 1; end
						end
					end
					if contact > returnBlock.number then 
						returnBlock.number = contact;
						returnBlock.x, returnBlock.y = x, y;
						returnBlock.width, returnBlock.height = argBlock.width, argBlock.height
					end
				end) ()
			end
		end

		return returnBlock
	end

	local function checkFitInTable(argTable, argWidth, argHeight)
		
		local Xmin, Xmax, Ymin, Ymax = 100, 0, 100, 0
		for y=1, #argTable do
			for x=1, #argTable[y] do
				if argTable[y][x] ~= 0 then
					if x < Xmin then Xmin = x end
					if y < Ymin then Ymin = y end
					if x > Xmax then Xmax = x end
					if y > Ymax then Ymax = y end
				end
			end
		end
		--		print("Xmin, Ymin, Xmax, Ymax = ", Xmin, Ymin, Xmax, Ymax)

		if (Xmax - Xmin +1) <= argWidth and (Ymax - Ymin + 1) <= argHeight then
			return true
		else
			return false
		end
	end

	local function cropTable(tbl)
		local Xmin, Xmax, Ymin, Ymax = 100, 0, 100, 0
		for y=1, #tbl do
			for x=1, #tbl[y] do
				if tbl[y][x] ~= 0 then
					if x < Xmin then Xmin = x end
					if y < Ymin then Ymin = y end
					if x > Xmax then Xmax = x end
					if y > Ymax then Ymax = y end
				end
			end
		end

		local croppedTable = {};
		for y=1, Ymax - Ymin + 1 do
			croppedTable[y] = {};
			for x=1, Xmax - Xmin + 1 do
				croppedTable[y][x] = tbl[y+Ymin-1][x+Xmin-1]
			end
		end
		return croppedTable;
	end


	-- 1. 주어진 level(단수)의 구구단중 하나를 선택, 가로세로도 random ex> 7단이고 3이 나왔으면 7x3 or 3x7
	local blocks = {}

	blocks[1] = makeBlock(level)

	-- 2. 20x20의 큰 tbl 중간에 배치
	for y=20, 20+blocks[1].height-1 do
		for x= 20, 20+blocks[1].width-1 do
			tbl[y][x] = 1
		end
	end


	for i=2, numberOfAnswers do
		iteration = 0;

		-- 해당 level로 블럭을 만들고
		repeat
			if iteration < 10 then
				blocks[i] = makeBlock(level)
			else
				blocks[i] = makeBlockLowerThanLevel(level)
			end

	--			print("block #", i, blocks[i].width, blocks[i].height)
			
			-- 적당한 위치를 찾아서 배치하고
			local placedBlock = findPlaceOnTable(blocks[i], tbl)
			if placedBlock.x ~= 0 then 
				for y=placedBlock.y, placedBlock.y + placedBlock.height-1  do
					for x= placedBlock.x, placedBlock.x + placedBlock.width-1  do
						tbl[y][x] = i
					end
				end
	--				print("placed Normaly at ", placedBlock.x, placedBlock.y)

				-- 사이즈가 맞으면 그만하고, 안맞으면 다시 반복 	
				if checkFitInTable(tbl, width, height)  then
					print("sizeFit!")
					break;
				else
					for y=1, 40 do
						for x=1, 40 do
							if tbl[y][x] == i then tbl[y][x] = 0 end
						end
					end					
				end				
			end
			
			iteration = iteration + 1;
	--		if iteration % 10 == 0 then print("iteration", iteration) end

		until iteration == 30

		print("iteration, block# = ", iteration, i)
	end

	--	printBlocks();

	tbl = cropTable(tbl)

	--	printBlocks();


	local function groupBlocks(tbl)
		local blockGroup = {}
		for i=1, #tbl do
			for j=1, #tbl[i] do
				local blockNumber = tbl[i][j]

				-- 새로 나온 숫자 블럭이면 table 생성 --
				if blockGroup[blockNumber] == nil then
					blockGroup[blockNumber] = {};
					blockGroup[blockNumber].startX = j;
					blockGroup[blockNumber].startY = i;
					blockGroup[blockNumber].endX = j;
					blockGroup[blockNumber].endY = i;
				else
					blockGroup[blockNumber].endX = j;
					blockGroup[blockNumber].endY = i;
				end

			end				
		end

		for i=1, #blockGroup do
			blockGroup[i].width = blockGroup[i].endX - blockGroup[i].startX + 1;
			blockGroup[i].height = blockGroup[i].endY - blockGroup[i].startY + 1;
	--	print(i, blockGroup[i].startX, blockGroup[i].startY, blockGroup[i].endX, blockGroup[i].endY, blockGroup[i].width, blockGroup[i].height);
		end
		

		return blockGroup;

	end

	-- genrate Answers
	local blockGroup = groupBlocks(tbl);
	local answerTable = {};

	for i=1, #blockGroup do
		answerTable[i] = {}
		answerTable[i].result = blockGroup[i].width * blockGroup[i].height;
		answerTable[i].width = blockGroup[i].width
		answerTable[i].height = blockGroup[i].height

	end

	local levelDataToReturn = {};

	levelDataToReturn.Q = {}

	for i=1, #tbl do
		levelDataToReturn.Q[i] = ""
		for j=1, #tbl[i] do
			levelDataToReturn.Q[i] = levelDataToReturn.Q[i] .. tbl[i][j]
		end
	end
	levelDataToReturn.A = answerTable;

	--	print ("levelData.Q = ", #levelDataToReturn.Q, levelDataToReturn.Q[1])

	return levelDataToReturn;

end	


local function makeRandomGame(width, height, numberOfAnswers)
	
	local tbl = {};
	for y=1, height do
		tbl[y] = {}
		for x=1, width do
			tbl[y][x] = 1
		end
	end

	local function groupBlocks(tbl)
		local blockGroup = {}
		for i=1, #tbl do
			for j=1, #tbl[i] do
				local blockNumber = tbl[i][j]

				-- 새로 나온 숫자 블럭이면 table 생성 --
				if blockGroup[blockNumber] == nil then
					blockGroup[blockNumber] = {};
					blockGroup[blockNumber].startX = j;
					blockGroup[blockNumber].startY = i;
					blockGroup[blockNumber].endX = j;
					blockGroup[blockNumber].endY = i;
				else
					blockGroup[blockNumber].endX = j;
					blockGroup[blockNumber].endY = i;
				end

			end				
		end

		for i=1, #blockGroup do
			blockGroup[i].width = blockGroup[i].endX - blockGroup[i].startX + 1;
			blockGroup[i].height = blockGroup[i].endY - blockGroup[i].startY + 1;
	--	print(i, blockGroup[i].startX, blockGroup[i].startY, blockGroup[i].endX, blockGroup[i].endY, blockGroup[i].width, blockGroup[i].height);
		end
		

		return blockGroup;

	end

	local function printBlocks()
		-- for checking
		for i=1, #tbl do
			local str= ""
			for j=1, #tbl[i] do
				str = str .. tbl[i][j]
			end
			print(str);
		end
	end

	local function cutHorizontal(blockGroup, groupNumber, cut)
	--		print("Cutting Horizontal, Group/Cut : ", groupNumber , cut)
		for y=blockGroup[groupNumber].startY + cut -1, blockGroup[groupNumber].endY do
			for x=blockGroup[groupNumber].startX, blockGroup[groupNumber].endX do
				tbl[y][x] = #blockGroup + 1;
			end
		end
	end

	local function cutVertical(blockGroup, groupNumber, cut)
	--		print("Cutting Vertical, Group/Cut : ", groupNumber , cut)
		for y=blockGroup[groupNumber].startY, blockGroup[groupNumber].endY do
			for x=blockGroup[groupNumber].startX+ cut -1, blockGroup[groupNumber].endX do
				tbl[y][x] = #blockGroup + 1;
			end
		end
	end

	for i=1, numberOfAnswers -1 do
		local blockGroup = groupBlocks(tbl);
		while true do

			local rand = math.random(1, #blockGroup)
			if blockGroup[rand].width == width then
				local cut = math.random(3, blockGroup[rand].width-2)
				cutVertical(blockGroup, rand, cut);
				break;
			elseif blockGroup[rand].height == height then
				local cut = math.random(3, blockGroup[rand].height-2)
				cutHorizontal(blockGroup, rand, cut);
				break;
			elseif blockGroup[rand].width >= 4 and blockGroup[rand].height >= 4 then				
				if rand%2 == 1 then
					local cut = math.random(2, blockGroup[rand].height)
					cutHorizontal(blockGroup, rand, cut)
				else
					local cut = math.random(2, blockGroup[rand].width)
					cutVertical(blockGroup, rand, cut)
				end
				break;
			elseif blockGroup[rand].width < 4 and blockGroup[rand].height >= 4 then
				local cut = math.random(2, blockGroup[rand].height)
				cutHorizontal(blockGroup, rand, cut)
				break;
			elseif blockGroup[rand].width >= 4 and blockGroup[rand].height < 4 then
				local cut = math.random(2, blockGroup[rand].width)
				cutVertical(blockGroup, rand, cut)
				break;
			end
		end

	--		printBlocks();

	end

	-- genrate Answers
	local blockGroup = groupBlocks(tbl);
	local answerTable = {};

	for i=1, #blockGroup do
		answerTable[i] = {};
		answerTable[i].result = blockGroup[i].width * blockGroup[i].height;
		answerTable[i].width = 0 -- blockGroup[i].width
		answerTable[i].height = 0 -- blockGroup[i].height
	end

	local levelDataToReturn = {};

	levelDataToReturn.Q = {}

	for i=1, height do
		levelDataToReturn.Q[i] = ""
		for j=1, width do
			levelDataToReturn.Q[i] = levelDataToReturn.Q[i] .. "1"
		end
	end
	levelDataToReturn.A = answerTable;

	--	print ("levelData.Q = ", #levelDataToReturn.Q, levelDataToReturn.Q[1])

	return levelDataToReturn;
	

end



local function createGameBlock(levelData)

	local startX, startY, currentX, currentY = 0, 0, 0, 0;

	local blocks = {};
	local answers = {};
	local result;


	local obj = display.newGroup();

	obj.fillSelectedBlocks = function () 

		local smallX = (startX > currentX) and currentX or startX
		local largeX = (startX > currentX) and startX or currentX
		local smallY = (startY > currentY) and currentY or startY
		local largeY = (startY > currentY) and startY or currentY

		
		for y=1,#blocks do
			for x=1,#blocks[y] do
				if blocks[y][x].confirmNumber ~= 0 and x>=smallX and x<= largeX and y >= smallY and y <= largeY then
					return;
				end
			end
		end

		result = (largeX-smallX+1)*(largeY-smallY+1)
		
		local width = largeX-smallX+1
		local height = largeY-smallY+1
		
		result = width * height;

		G_equation = width .. " X " .. height .. 
												" = " .. result

		local matchAnswer = false;

		-- baloon and answer highlighting --
		for k=1, #answers do

			-- answers에 가로/세로가 정해져 있다면 - 즉, 숫자의 곱이 정해진 문제라면 (step by step, random on levels)	
			if answers[k].garo ~= 0 then 
				if answers[k].solved == false and matchAnswer == false and 
					((answers[k].garo == width and answers[k].sero == height) or (answers[k].garo == height and answers[k].sero == width) ) then
					matchAnswer = k
					answers[k].setActive();
				elseif answers[k].solved == false then
					answers[k].setInactive();
				end
			else -- 가로세로 상관없이 : ex> 12를 3x4로 푸나, 2x6으로 푸나 상관 없는 케이스. - 마지막 랜덤 게임에만 해당.
				if answers[k].number == result and answers[k].solved == false and matchAnswer == false then
					matchAnswer = k
					answers[k].setText(G_equation)
					answers[k].setActive();
				elseif answers[k].number ~= result and answers[k].solved == false then
					--print(G_equation .. "???")
					answers[k].setText(answers[k].number)
					answers[k].setInactive();
				end
			end
		end

		for i=1,#blocks do
			for j=1,#blocks[i] do

				if j>= smallX and j<= largeX and i >= smallY and i <= largeY then
					if matchAnswer then
						blocks[i][j].setFace(matchAnswer)
						blocks[i][j].select = true;
					else
		--						blocks[i][j].setColor();
						blocks[i][j].setFace(-1)
						blocks[i][j].select = false;
					end
				elseif blocks[i][j].confirmNumber == 0 then
					blocks[i][j].reset();
					blocks[i][j].select = false;
				end

				if j == currentX and i == currentY then
					
					blocks[i][j].setBaloonText(G_equation)
				else
					blocks[i][j].removeBaloon();
				end
			end
		end

	end

	obj.cancelBlocks = function (x,y)
		local cancelNumber = blocks[y][x].confirmNumber;
		for i=1,#blocks do
			for j=1,#blocks[i] do
				if blocks[i][j].confirmNumber == cancelNumber then
					blocks[i][j].reset();
					blocks[i][j].confirmNumber = 0;
				end
			end
		end
		for i=1, #answers do
			if answers[i].playStep == cancelNumber then
				answers[i].playStep = 0;
				answers[i].solved = false;
				answers[i].setText(answers[i].number)
				answers[i].setInactive();
			end
		end
	end

	obj.callBack = function(x, y, phase)

		-- 아직 같은 블럭이면 계산 안하고 return
		


		if phase == "began" then
			startX, startY = x, y;
			currentX, currentY = x, y;
			
			if x~= 0 and blocks[y][x].confirmNumber ~= 0 then
				obj.cancelBlocks(x,y);
				result = 0; -- used 'ended' event of cancel click.
			else
				obj.fillSelectedBlocks()
			end
			
		elseif phase == "moved" then
			-- 다른 블럭까지 이동하지 않았으면 계산식을 줄이기 위해 중단.
			if x == currentX and y == currentY then
				return;
			end

			if startX == 0 then
				startX, startY = x, y;
			end
			currentX, currentY = x, y;
			obj.fillSelectedBlocks();
		elseif phase == "ended" then

			

			for i=1,#blocks do
				for j=1,#blocks[i] do
					if blocks[i][j].select == true then
						blocks[i][j].confirmNumber = G_playStep;
						blocks[i][j].select = false;
					elseif blocks[i][j].confirmNumber == 0 then
						blocks[i][j].reset();
					end
					blocks[i][j].removeBaloon();
				end
			end
			startX, startY = 0, 0;

			local unsolved = false;
			for i=1, #answers do

				if answers[i].number == result and answers[i].solved == false then
					answers[i].solved = true;
					answers[i].playStep = G_playStep
					G_playStep = G_playStep + 1;
					break;
				end
			end

			for i=1, #answers do
				if answers[i].solved == false then
					unsolved = true;
				end

			end

			-- if level complete
			if unsolved == false then

		--				displayGoodJob();
				for i=1,#blocks do
					for j=1,#blocks[i] do
						local targetNumber
						for k=1, #answers do
							if answers[k].playStep == blocks[i][j].confirmNumber then
								targetNumber = k
								break;
							end
						end
						print("block attacking to ", i, j, targetNumber)
						if blocks[i][j].isVisible then 
							blocks[i][j].attack(answers[targetNumber]) 
						end
					end
				end
				timer.performWithDelay(1000, function () 
												for k=1, #answers do
													answers[k].explode();
												end

												for i=1,#blocks do
													for j=1,#blocks[i] do
														if blocks[i][j].isVisible then 
															blocks[i][j].flyAway() 
														end
													end
												end


											end )
				timer.performWithDelay(1000+1450+250, function () G_monster.explode() end )

				if G_currentLevel == 0 then
					timer.performWithDelay(4000, function () 
							G_gameBlocks:removeSelf();

							--G_gameBlocks = createGameBlock(makeRandomGame(G_randomSize, G_randomSize, G_answerNumber))
							if G_randomStep == 0 then
								G_levelData = makeRandomGame(G_randomSize,G_randomSize, G_answerNumber);
							else
								G_levelData = makeRandomGameOnLevel(G_randomSize, G_randomSize,G_answerNumber, G_randomStep)
							end
							G_gameBlocks = createGameBlock(G_levelData)
							G_monster:setStatusNormal();
							 end) 
				else
					local options =	{
							params = {
								level = G_currentLevel + 1,
								start = true
							}
						}

					timer.performWithDelay(4000, function () 
						G_gameBlocks:removeSelf();
						if G_currentLevel < #G_stepByStepLevelData then 
							G_currentLevel = G_currentLevel + 1
						end
				        G_levelText.text = G_currentLevel;
				        G_gameBlocks = createGameBlock(G_stepByStepLevelData[G_currentLevel])
							G_monster:setStatusNormal();
						--storyboard.gotoScene( "sceneLevelSelect" , options) 
						end)
				end
				
			end

		end

	end



	local function maxTable(tbl)
		local max = 0;
		for i=1, #tbl do
			if string.len(tbl[i]) > max then
				max = string.len(tbl[i])
			end
		end
		return max;
	end

	-- block size와 위치 계산용
	local levelSize = #levelData.Q
	for y=1, #levelData.Q do
		if levelSize < string.len(levelData.Q[y]) then
			levelSize = string.len(levelData.Q[y])
		end
	end
	local blockSize = math.floor(screenH/levelSize) - 5
	if blockSize >= 80 then blockSize = 80 end


	local offsetX = screenW * 0.62 - maxTable(levelData.Q)/2*blockSize
	local offsetY = screenH * 0.5 - #levelData.Q/2*blockSize

	
	
	local blockNumber = 1;
	for y=1, #levelData.Q do
		for x=1, string.len(levelData.Q[y]) do
			if x == 1 then
				blocks[y] = {}
			end
			
			blocks[y][x] = block.new(blockNumber, x, y, obj.callBack);
			blocks[y][x].x = offsetX + blockSize*(x-1)
			blocks[y][x].y = offsetY + blockSize*(y-1)
			blocks[y][x].select = false;
			blocks[y][x].confirmNumber = 0;
			blocks[y][x].xScale = blockSize/100
			blocks[y][x].yScale = blockSize/100
			G_playStep = 1;

			if levelData.Q[y]:sub(x, x) == "0" or levelData.Q[y]:sub(x, x) == "." then
				blocks[y][x].confirmNumber = 100;
			end;
			blocks[y][x].isVisible = false;

			obj:insert(blocks[y][x]);

			blockNumber = blockNumber + 1;
			timer.performWithDelay(blockNumber*5, function () 
													if blocks[y][x].confirmNumber == 0 then
														blocks[y][x].isVisible = true;
													end
										end )
		end
	end

	for y=1, #levelData.Q do
		for x=1, string.len(levelData.Q[y]) do
			blocks[y][x].start();
		end
	end
	-- display Answers
	print("levelDAta", #levelData.A)
	for i=1, #levelData.A do
		local equationString 
		if levelData.A[i].height ~= 0 then 
			equationString = levelData.A[i].height .. " x " .. levelData.A[i].width .. " = " .. levelData.A[i].result
		else
			equationString = levelData.A[i].result
		end
		answers[i] = answerBomb.new(equationString, G_colorTable[i]);
		answers[i].number = levelData.A[i].result
		answers[i].sero = levelData.A[i].height
		answers[i].garo = levelData.A[i].width
		answers[i].solved = false;	
		answers[i].playStep = 0;
		if #levelData.A > 10 then
			answers[i].xScale = 9/#levelData.A
			answers[i].yScale = 9/#levelData.A
			answers[i].x = screenW * 0.13
			answers[i].y = 280 + 9/#levelData.A*40*i
		else
			answers[i].x = screenW * 0.13
			answers[i].y = 280 + i * 40
		end
		obj:insert(answers[i])
	end;

	startX, startY, currentX, currentY = 0, 0, 0, 0

	obj.remove = function ()
		display.remove(obj)
	end

	return obj;

end




-- Called when the scene's view does not exist:
function scene:createScene( event )
        local group = self.view
		
        -----------------------------------------------------------------------------

        --      CREATE display objects and add them to 'group' here.
        --      Example use-case: Restore 'group' from previously saved state.

        -----------------------------------------------------------------------------
		
		--local bg = display.newRect(group, 0, 0, screenW, screenH );
    	--bg:setFillColor(255, 255, 255) 

    	--local bg = display.newImage(group, "images/bg_play.png");
    	bg = display.newRect(group, screenCenterX, screenCenterY, screenW, screenH );
    	bg:setFillColor(1) 

    	function bg:touch(event)
    		if event.phase == "ended" then
    			G_gameBlocks.callBack(0,0,"ended");
    		end

		end
		
		bg:addEventListener("touch", bg)

		G_levelText = display.newText(group, "", 30, 40, "Futura-Medium", 40)
		G_levelText:setFillColor(0);

	--[[	
		local line = display.newLine(group, 80, 0, 80, screenH)
		line:setStrokeColor(0.5)
		line.strokeWidth = 5;
	--]]

		local function buttonEvent( event )
		    local phase = event.phase 

		    if "ended" == phase then
		    	if event.target.id == "back" then
					storyboard.gotoScene( storyboard.getPrevious(), {effect = "fade", time = 100, params = {} })
				elseif event.target.id == "refresh" then
					
					G_gameBlocks.remove();
			        G_gameBlocks = createGameBlock(G_levelData)
			        group:insert(G_gameBlocks);
				end
		    end
		end

		-- Create the button
		local backButton = widget.newButton
		{
		    left = 12,
		    top = 600,
		    width = 50,
		    height = 50,
		    defaultFile = "images/backButton.png",
		    overFile = "images/backButton.png",
		    id = "back",
		    onEvent = buttonEvent,
		}
		group:insert(backButton)

		local refreshButton = widget.newButton
		{
		    left = 12,
		    top = 680,
		    width = 50,
		    height = 50,
		    defaultFile = "images/refreshButton.png",
		    overFile = "images/refreshButton.png",
		    id = "refresh",
		    onEvent = buttonEvent,
		}
		group:insert(refreshButton)

		G_monster = monster.new();
		G_monster:translate(screenW * 0.13, screenCenterY)
		G_monster:scale(2, 2)

		group:insert(G_monster)

end	


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. start timers, load audio, start listeners, etc.)

        -----------------------------------------------------------------------------
	
        if event.params.randomSize == 0 then -- step by step level
        	G_stepByStepLevelData = event.params.levelData;

	        G_currentLevel = event.params.level;
	        if G_currentLevel > #G_stepByStepLevelData then
	        	G_currentLevel = #G_stepByStepLevelData;
	        end

	        G_levelText.text = G_currentLevel;
	        G_gameBlocks = createGameBlock(G_stepByStepLevelData[G_currentLevel])

	    elseif event.params.randomStep == 0 then  -- random with Fixed Square
	    	G_randomSize = event.params.randomSize;
        	G_answerNumber = event.params.numberOfAnswers;
        	G_randomStep = 0;
			G_levelData = makeRandomGame(G_randomSize,G_randomSize, G_answerNumber);
			G_currentLevel = 0;
			G_gameBlocks = createGameBlock(G_levelData)

	    else
       		G_randomSize = event.params.randomSize;
        	G_answerNumber = event.params.numberOfAnswers;
        	G_randomStep = event.params.randomStep;

        	G_levelData = makeRandomGameOnLevel(G_randomSize, G_randomSize,G_answerNumber, G_randomStep)
			G_currentLevel = 0;
			G_gameBlocks = createGameBlock(G_levelData)

	    end
        group:insert(G_gameBlocks)
	
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)

        -----------------------------------------------------------------------------
        group:remove(G_gameBlocks);
        G_gameBlocks:removeSelf();

end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. remove listeners, widgets, save state, etc.)

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end



---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene