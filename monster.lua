module(..., package.seeall)

function new()

	local obj = display.newGroup();
	
	local sheetInfo = require("monster_sheet")
	local myImageSheet = graphics.newImageSheet( "images/monster_sheet.png", sheetInfo:getSheet() )

	local sequenceData =
	{	
		{
		    name="normal",
		    frames= {1,1,1,1,1,1,1,1,1,2},
		    time= 1000,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "bounce"    -- Optional. Values include: "forward","bounce"
		},
		{
		    name="explosion",
		    frames= { 3,7,9,8,11,10,12,5,6,5},
		    time= 600,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 1,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "forward"    -- Optional. Values include: "forward","bounce"
		},

	}

	obj.monster = display.newSprite(obj, myImageSheet, sequenceData )
	obj.monster:setSequence("normal");
	obj.monster:play()

	obj.monster:scale(0.4, 0.4)

	obj.explode = function ()
		obj.monster:setSequence("explosion");
		obj.monster:play();
	end

	obj.setStatusNormal = function ()
		obj.monster:setSequence("normal");
		obj.monster:play();
	end

	return obj;

end
