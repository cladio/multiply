local storyboard = require( "storyboard" )
local widget = require("widget")
local dataIO = require("dataIO")
local user = require ("user")

local printEmpty = function()	return; end
local DEBUG = printEmpty; 



local scene = storyboard.newScene()

----------------------------------------------------------------------------------
-- 
--      NOTE:
--      
--      Code outside of listener functions (below) will only be executed once,
--      unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

local levelData
local storedStep = 6

local enabledLevel = 1;

local levelButton = {};

-- Called when the scene's view does not exist:
function scene:createScene( event )
    local group = self.view
	
    -----------------------------------------------------------------------------

    --      CREATE display objects and add them to 'group' here.
    --      Example use-case: Restore 'group' from previously saved state.

    -----------------------------------------------------------------------------
	

	local bg = display.newRect(group, screenCenterX, screenCenterY, screenW, screenH );
	bg:setFillColor(1, 1, 1) 

    function onButtonTouch(event)
		local target = event.target;
		
		if event.phase == "ended" then

			if event.target.level == "random" then 
                local options = {
                            effect = "fade",
                            time = 300,
                            params = {
                                level = event.target.level,
                                levelData = levelData;
                                randomSize = 12,
                                randomStep = storedStep,
                                numberOfAnswers = 8 
                            }
                        }
                storyboard.gotoScene( "sceneTileGame" , options )
            elseif event.target.level <= enabledLevel then

				local options =	{
							effect = "fade",
							time = 300,
							params = {
								level = event.target.level,
								levelData = levelData;
                                randomSize = 0,
                                randomStep = 0,
                                numberOfAnswers = 0 
							}
						}
				storyboard.gotoScene( "sceneTileGame" , options )
			end

			return true;
		end

	end


    local radius, theta
    radius = screenH * 0.5 - 30
    theta = 45


    for i=1, 100 do

        local temp_radius = radius -- radius * (math.sin(math.rad((theta-20)*4))*0.1 + 1.05)
        local x = - temp_radius * math.cos(math.rad(theta))
        x = x + screenW * 0.5 + 50

        local y = temp_radius * math.sin(math.rad(theta))
        y = screenH*0.5 - y

        levelButton[i] = user.newRoundButton{
            id = "i",
            radius = 20,  
            strokeWidth = 1,
            strokeColor = {0.6},

            defaultColor = {0.6, 0.6, 0.6}, -- color.pickTable("gray20"),
            overColor = {50/255, 50/255, 230/255},

            label = i,
            labelOffsetX = 0,
            labelOffsetY = 0, 
            font = "HelveticaNeue-UltraLight",
            fontSize = 20,
            fontDefaultColor = {1,1,1},
            fontOverColor = {1,1,1},

            onEvent = onButtonTouch;
        }

        levelButton[i].level = i;

        levelButton[i].x = x
        levelButton[i].y = y
        group:insert(levelButton[i])


        radius = radius - 0.8 - i/30

        theta = theta + math.deg(math.asin(55/radius))

    end

    randomLevel = user.newRoundButton{
        id = "random",
        radius = 50,  
        strokeWidth = 1,
        strokeColor = {150, 150, 150},

        defaultColor = {50/255, 50/255, 230/255},-- color.pickTable("gray20"),
        overColor = {50/255, 50/255, 230/255},

        label = "random",
        labelOffsetX = 0,
        labelOffsetY = 0, 
        font = "HelveticaNeue-UltraLight",
        fontSize = 20,
        fontDefaultColor = {1,1,1},
        fontOverColor = {1,1,1},

        onEvent = onButtonTouch;
    }

    randomLevel.level = "random";

    randomLevel.x = screenW*0.5 + 50
    randomLevel.y = screenH*0.5 - 20
    group:insert(randomLevel)


    local function buttonEvent( event )
        local phase = event.phase 

        if "ended" == phase then
            if event.target.id == "back" then
                storyboard.gotoScene( "sceneStepByStep" )
            end
        end
        return true;
    end

    -- Create the button
    local backButton = widget.newButton
    {
        left = 20,
        top = 20,
        width = 50,
        height = 50,
        defaultFile = "images/backButton.png",
        overFile = "images/backButton.png",
        id = "back",
        onEvent = buttonEvent,
    }
    group:insert(backButton)

end	


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------
        if event.params.step then
            storedStep = event.params.step
        end

        levelData = dataIO.readLevelFile("level/".. storedStep  .. ".data")

        enabledLevel = #levelData

        for i=1, 100 do 
            if i <= enabledLevel then 
               levelButton[i].setColor("over");
            else
               levelButton[i].setColor("default");
            end                
        end
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. start timers, load audio, start listeners, etc.)

        -----------------------------------------------------------------------------
        
        if event.params.start then
            local options = {
                                effect = "fade",
                                time = 100,
                                params = {
                                    level = event.params.level,
                                    levelData = levelData;
                                    randomSize = 0,
                                    numberOfAnswers = 0,
                                    randomStep = 0,
                                }
                            }
            storyboard.gotoScene( "sceneTileGame" , options )
        end
		
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)

        -----------------------------------------------------------------------------

end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. remove listeners, widgets, save state, etc.)

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end



---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene
