local storyboard = require( "storyboard" )
local block = require("block_bee")
local answerCircle = require("answerCircle")
local widget = require("widget")


local printEmpty = function()	return; end
local DEBUG = printEmpty; 



local scene = storyboard.newScene()

----------------------------------------------------------------------------------
-- 
--      NOTE:
--      
--      Code outside of listener functions (below) will only be executed once,
--      unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

local G_levelText;
local G_gameBlocks;
local G_currentLevel = 0
local G_levelData;
local G_equation;

local numberLevel = {};


local G_currentNumber

local G_colorTable = { 
	{238, 115, 97},
	{245, 155, 50}, 
	{250, 230, 50},
	{192, 212, 35}, 
	{140, 186, 126},
	{142, 177, 215},
	{157, 140, 196},
	{205, 131, 172},
	{238, 115, 97},
	{100, 100, 100}
}
local G_defaultBaloonColor = {100, 100, 100}
local G_playStep = 0;



local function createJumpingBlock(levelNumber)

	local blocks = {};
	local answers = {};

	local obj = display.newGroup();

	obj.fillSelectedBlocks = function () 

		local smallX = (startX > currentX) and currentX or startX
		local largeX = (startX > currentX) and startX or currentX
		local smallY = (startY > currentY) and currentY or startY
		local largeY = (startY > currentY) and startY or currentY

		
		for y=1,#blocks do
			for x=1,#blocks[y] do
				if blocks[y][x].confirmNumber ~= 0 and x>=smallX and x<= largeX and y >= smallY and y <= largeY then
					return;
				end
			end
		end

		result = (largeX-smallX+1)*(largeY-smallY+1)
		G_equation = tostring(largeX-smallX+1) .. " X " .. tostring(largeY-smallY+1) .. 
												" = " .. tostring(result)

		local matchAnswer = false;

		-- baloon and answer highlighting --
		for k=1, #answers do
			if answers[k].number == result and answers[k].solved == false and matchAnswer == false then
				matchAnswer = k
				answers[k].setExpandedText(G_equation)

			elseif answers[k].number ~= result and answers[k].solved == false then
				answers[k].setText(answers[k].number)
			end
		end

		for i=1,#blocks do
			for j=1,#blocks[i] do

				if j>= smallX and j<= largeX and i >= smallY and i <= largeY then
					if matchAnswer then
						blocks[i][j].setColor(G_colorTable[matchAnswer])
						blocks[i][j].select = true;
					else
						blocks[i][j].setColor();
						blocks[i][j].select = false;
					end
				elseif blocks[i][j].confirmNumber == 0 then
					blocks[i][j].reset();
					blocks[i][j].select = false;
				end

				if j == currentX and i == currentY then
					
					blocks[i][j].setBaloonText(G_equation)
				else
					blocks[i][j].removeBaloon();
				end
			end
		end

	end

	obj.selectBlock = function (x, y)
		for i=1,#blocks do
			for j=1,#blocks[i] do
				if blocks[i][j].confirm == false then
					blocks[i][j]:reset();
				end
			end
		end;
		if blocks[y][x].number == answers[G_currentAnswer].answerNumber then
			blocks[y][x].setColor(G_colorTable[G_currentAnswer])
		else
			blocks[y][x].setColor({150, 150, 150})
		end
	end

	local refTable = { "C3", "D3", "E3", "F3", "G3", "A3", "B3", "C4" }

	obj.sounds = {};
	for i=1, #refTable do
		obj.sounds[i] = audio.loadSound("sound/" .. refTable[i] .. ".mp3");
	end

	obj.callBack = function(x, y, phase)

--		if phase == "began" or phase == "moved" then
--			obj.selectBlock(x, y)
--		else

		if phase == "began" then
			if x == 0 then
				return;
			end

			for i=1,#blocks do
				for j=1,#blocks[i] do
					if blocks[i][j].confirm == false then
						blocks[i][j]:reset();
					end
					blocks[i][j]:removeBaloon();
				end
			end;
			if blocks[y][x].number == answers[G_currentAnswer].answerNumber then

				blocks[y][x].setColor(G_colorTable[G_currentAnswer])
				blocks[y][x].confirm = true;
				blocks[y][x].setBaloonText(G_currentLevel .. " x " .. answers[G_currentAnswer].answerNumber/G_currentLevel .. " = " .. answers[G_currentAnswer].answerNumber)

				answers[G_currentAnswer]:setFillColor(255)
				answers[G_currentAnswer].outLine:setFillColor(G_colorTable[G_currentAnswer][1], G_colorTable[G_currentAnswer][2], G_colorTable[G_currentAnswer][3]);
				answers[G_currentAnswer].outLine.strokeWidth = 0;

				if G_currentAnswer == 9 then
					audio.play(obj.sounds[5])
				elseif G_currentAnswer == 10 then
					audio.play(obj.sounds[8])
				else
					audio.play(obj.sounds[G_currentAnswer])
				end

				G_currentAnswer = G_currentAnswer + 1
				
				if G_currentAnswer == 11 then
					G_currentAnswer = 2;
					
					if G_currentLevel ~= 9 then
						G_currentLevel = G_currentLevel + 1;
					end

					displayGoodJob();
					timer.performWithDelay(2000, function () 
						storyboard.reloadScene();
			         end)
				else
					answers[G_currentAnswer].outLine.isVisible = true;
				end
			end


		end

			return true;
	end


	local blockSize = 60;

	local offsetX = screenW * 0.62 - 10/2*blockSize
	local offsetY = screenH * 0.5 - levelNumber/2*blockSize



	local blockNumber = 1;
	for y=1, levelNumber do
		for x=1, 10 do
			if x == 1 then
				blocks[y] = {}
			end
			
			blocks[y][x] = block.new(blockNumber, x, y, obj.callBack);
			blocks[y][x].x = offsetX + blockSize*(x-1)
			blocks[y][x].y = offsetY + blockSize*(y-1)
			blocks[y][x].text.isVisible = true;
			blocks[y][x].select = false;
			blocks[y][x].confirm = false;
			blocks[y][x].isVisible = false;

			obj:insert(blocks[y][x]);

			blockNumber = blockNumber + 1;
			timer.performWithDelay(blockNumber*5, function () 
														blocks[y][x].isVisible = true;
													end )
		end
	end

	-- display Answers

	for i=1, 10 do
		answers[i] = display.newText(obj, "", 30, 40, "Futura-Medium", 30)
		answers[i].text = levelNumber .. " X " .. i .. " = " .. levelNumber*i;
		answers[i].answerNumber = levelNumber*i;
		answers[i]:setFillColor(200);
		answers[i]:setReferencePoint(display.TopLeftReferencePoint)
		answers[i].x = 110
		answers[i].y = i * 70 -30
		answers[i].solved = false;	
		answers[i].outLine = display.newRoundedRect(obj, answers[i].x-10, answers[i].y - 5, 180, 50, 5 )
		answers[i].outLine:setFillColor(0,0,0,0)
		answers[i].outLine:setStrokeColor(150)
		answers[i].outLine.strokeWidth = 2;
		answers[i].outLine.isVisible = false;
		answers[i].outLine:toBack();
	end;

	G_currentAnswer = 1;
	answers[G_currentAnswer].outLine.isVisible = true;

	obj.remove = function ()
		display.remove(obj)
	end

	return obj;

end




-- Called when the scene's view does not exist:
function scene:createScene( event )
        local group = self.view
		
        -----------------------------------------------------------------------------

        --      CREATE display objects and add them to 'group' here.
        --      Example use-case: Restore 'group' from previously saved state.

        -----------------------------------------------------------------------------
		
		local bg = display.newRect(group, 0, 0, screenW, screenH );
    	bg:setFillColor(255, 255, 255) 

    	function bg:touch(event)
    		if event.phase == "ended" then
    			G_gameBlocks.callBack(0,0,"ended");
    		end

		end
		
		bg:addEventListener("touch", bg)

		local line = display.newLine(group, 80, 0, 80, screenH)
		line:setColor(128)
		line.width = 5;

		local function buttonEvent( event )
		    local phase = event.phase 

		    if "ended" == phase then
		    	if event.target.id == "back" then
					storyboard.gotoScene( "sceneGameSelect" , options)
				elseif event.target.id == "refresh" then
					
					G_gameBlocks.remove();
			        G_gameBlocks = createJumpingBlock(event.target.number)
			        group:insert(G_gameBlocks);
				end
		    end
		end



		function onTouchNumber(event)

			
			if event.phase == "began" or event.phase == "moved" then
				for i=2,9 do
					numberLevel[i]:setFillColor(200);
				end
				event.target:setFillColor(50);
			elseif event.phase == "ended" then
				G_currentLevel = event.target.number;
				storyboard.reloadScene();
--[[				for i=2,9 do
					numberLevel[i]:setFillColor(200);
				end
				event.target:setFillColor(50);
				if G_gameBlocks then
					G_gameBlocks:remove();
				end

				G_gameBlocks = createJumpingBlock(event.target.number)
				group:insert(G_gameBlocks)
--]]
			end

			return true;
		end
		
		
		for i=2, 9 do
			numberLevel[i] = display.newText(group, i, 30, 40, "Futura-Medium", 40)
			numberLevel[i].number = i;
			numberLevel[i]:setFillColor(200);
			numberLevel[i]:setReferencePoint(display.centerReferencePoint)
			numberLevel[i].x = 40
			numberLevel[i].y = i*60 - 50;
			numberLevel[i]:addEventListener("touch", onTouchNumber)
		end

		-- Create the button
		local backButton = widget.newButton
		{
		    left = 12,
		    top = 600,
		    width = 50,
		    height = 50,
		    defaultFile = "images/backButton.png",
		    overFile = "images/backButton.png",
		    id = "back",
		    onEvent = buttonEvent,
		}
		group:insert(backButton)

		local refreshButton = widget.newButton
		{
		    left = 12,
		    top = 680,
		    width = 50,
		    height = 50,
		    defaultFile = "images/refreshButton.png",
		    overFile = "images/refreshButton.png",
		    id = "refresh",
		    onEvent = buttonEvent,
		}
		group:insert(refreshButton)


		printMem();
end	


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. start timers, load audio, start listeners, etc.)

        -----------------------------------------------------------------------------
		if G_currentLevel == 0 then
			G_currentLevel = 2;
		end
		for i=2,9 do
			numberLevel[i]:setFillColor(200);
		end
		numberLevel[G_currentLevel]:setFillColor(50);
		G_gameBlocks = createJumpingBlock(G_currentLevel)

        group:insert(G_gameBlocks)

end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)

        -----------------------------------------------------------------------------
        group:remove(G_gameBlocks);
        G_gameBlocks:removeSelf();

end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. remove listeners, widgets, save state, etc.)

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end



---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene