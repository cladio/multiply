module(..., package.seeall)

function new(number, xAxis, yAxis, callBack)

	local obj = display.newGroup();

	obj.number = number;
	obj.xAxis = xAxis;
	obj.yAxis = yAxis;

	--obj.block = display.newRect(obj,0, 0, 58, 58);
	obj.block = display.newImageRect(obj, "images/beeHouse.png", 100, 100);
	obj.block.x = 29;
	obj.block.y = 29;

	obj.block:setFillColor(0.8)


	local sheetInfo = require("bee_sheet")
	local myImageSheet = graphics.newImageSheet( "images/bee_sheet.png", sheetInfo:getSheet() )

	local sequenceData =
	{	
		{
		    name="stay",
		    frames= { 1, 1, 1, 1, 1, 1, 2},
		    time= 2000,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "forward"    -- Optional. Values include: "forward","bounce"
		},
		{
		    name="selected",
		    frames= { 3, 5, 6, 7, 8, 9, 10, 11, 12, 4},
		    time= 700,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "bounce"    -- Optional. Values include: "forward","bounce"
		},

		{
		    name="confirmed",
		    frames= { 13, 14, 15, 16},
		    time= 1000,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "forward"    -- Optional. Values include: "forward","bounce"
		},

		{
		    name="fly",
		    frames= { 17, 18, 19, 20, 21},
		    time= 300,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 1,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "forward"    -- Optional. Values include: "forward","bounce"
		},

		{
		    name="attack",
		    frames= { 22, 23, 24, 25},
		    time= 200,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "bounce"    -- Optional. Values include: "forward","bounce"
		},
		{
		    name="rightToZero",
		    frames= { 21, 20, 19, 18, 17},
		    time= 200,        -- Optional. In ms.  If not supplied, then sprite is frame-based.
		    loopCount = 0,    -- Optional. Default is 0 (loop indefinitely)
		    loopDirection = "bounce"    -- Optional. Values include: "forward","bounce"
		},
	}

	obj.beeanimation = display.newSprite(obj, myImageSheet, sequenceData )
	obj.beeanimation.x = obj.beeanimation.x + 29;
	obj.beeanimation.y = obj.beeanimation.y + 29;	
	obj.beeanimation:setSequence("stay");
	obj.beeanimation.xScale = 1
	obj.beeanimation.yScale = 1


	obj.baloon = display.newRoundedRect(obj, 50,-30, 140, 50, 5)
	obj.baloon.strokeWidth = 0;
	obj.baloon:setFillColor(color255to1(20, 104, 255));
	obj.baloon.text = display.newText(obj, "", obj.baloon.x, obj.baloon.y, "Futura-Medium", 24)
	obj.baloon.text:setFillColor(1);
--	obj.baloon.text:setReferencePoint(display.centerReferencePoint);
--	obj.baloon.text.x = obj.baloon.x;
--	obj.baloon.text.y = obj.baloon.y;

	obj.baloon.isVisible = false;
	obj.baloon.text.isVisible = false;

	obj.setBaloonText = function(text)
		obj.baloon.isVisible = true;
		obj.baloon.text.isVisible = true;
		obj.baloon.text.text = text
--		obj.baloon.text:setReferencePoint(display.centerReferencePoint);
--		obj.baloon.text.x = obj.baloon.x;
--		obj.baloon.text.y = obj.baloon.y;
	end

	obj.setBaloonColor = function(tbl)
		obj.baloon:setFillColor(tbl[1], tbl[2], tbl[3])
	end

	obj.removeBaloon = function()
		obj.baloon.isVisible = false;
		obj.baloon.text.isVisible = false
	end

	obj.setFace = function (faceNumber)
		if faceNumber then
			
			if faceNumber > 6 then
				faceNumber = faceNumber % 7 + 1
			end

			if faceNumber == -1 then
				if obj.beeanimation.sequence ~= "selected" then 
					obj.beeanimation:setSequence("selected");
					obj.beeanimation:play();
					obj.block:setFillColor(0.4)
				end
			else
				obj.beeanimation:setSequence("confirmed");
				obj.beeanimation:play();
				obj.block:setFillColor(G_colorTable[faceNumber][1], G_colorTable[faceNumber][2], G_colorTable[faceNumber][3])
			end

		end
	end

	obj.start = function()
		obj.beeanimation:play();
	end

	obj.reset = function()
		obj.baloon.isVisible = false;
		obj.baloon.text.isVisible = false;
		
		obj.beeanimation:setSequence("stay");
		obj.beeanimation:play();
		obj.block:setFillColor(0.8)


	end

	obj.attack = function (target)

		obj.block.isVisible = false;
		
		local xOffset =  20
		local yOffset =  -20

		local attackPointX = target.x + xOffset
		local attackPointY = target.y + yOffset
		
		if attackPointX < obj.x - 20 then
			obj:scale(-1, 1)
		end

		--obj.beeanimation:pause();
		obj.beeanimation:setSequence("fly")
		obj.beeanimation:play();


		local deltaX = (attackPointX - obj.x)*0.02
		local deltaY = (attackPointY - obj.y)*0.08

		local function moveBee(event)
			if (math.abs(attackPointX - obj.x ) + math.abs(attackPointY - obj.y)) < 80 then
				obj.beeanimation:setSequence("attack")
				obj.beeanimation:play();
				Runtime:removeEventListener("enterFrame", moveBee) 

			else
				deltaY = (attackPointY - obj.y)*0.08
				obj:translate(deltaX, deltaY)
			end
			--print(".")
		end


		Runtime:addEventListener("enterFrame", moveBee)

	end

	obj.flyAway = function (target)
		transition.to(obj, {time=2000, x=screenW, y= math.random(-50, 50), transition = inSine})
		obj.beeanimation:setSequence("rightToZero")
		obj.beeanimation:play();
		timer.performWithDelay(210, function () 
										obj:scale(-1, 1)
										obj.beeanimation:setSequence("fly")
										obj.beeanimation:play();
									end
									)
	end

	obj.remove = function()
		obj.isVisible = false;
	end
	
	--------------- Touch Event Handler -------------------

	local function onTouchBlock(event)
		local target = event.target;
		
		callBack(xAxis,yAxis,event.phase)

		return true;

	end
			
	obj.block:addEventListener("touch", onTouchBlock)

	return obj;

end


