local json = require("json")

module(..., package.seeall)


readJsonFile = function( filename, base )

	-- set default base dir if none specified
	if not base then base = system.DocumentsDirectory; end

	-- create a file path for corona i/o
	local path = system.pathForFile( filename, base )

	-- will hold contents of file
	local contents

	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "r" )
	if file then
	   -- read all contents of file into a string
	   contents = file:read( "*a" )
	   io.close( file )	-- close the file after using it
	else
		return nil;
	end
	
	local t = json.decode(contents)

	return t
end

writeJsonFile = function(table, filename, base )
	
	-- set default base dir if none specified
	if not base then base = system.DocumentsDirectory; end

	-- create a file path for corona i/o
	local path = system.pathForFile( filename, base )

	-- will hold contents of file
	local jsonString = json.encode(table)
	
	-- write to file
	local file = io.open( path, "w" )
	
	file:write(jsonString);
	
	io.close( file )

end

readLevelFile = function (filename)

	-- create a file path for corona i/o
	local path = system.pathForFile( filename, base )
	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "r" )
	
	local data = {};
	local currentLevel = 0

	if file then
	   -- read all contents of file into a string
		local lineString = file:read("*l")
		repeat

			local firstChar = lineString:sub(1,1)
			if firstChar == "[" then
				-- level number
				currentLevel = currentLevel + 1;
				data[currentLevel] = {};
				data[currentLevel].Q = {};
				data[currentLevel].A = {};

			elseif firstChar == "-" then
				-- answer
				local a, b
				a = lineString:sub(2, lineString:find("x")-1)
				b = lineString:sub(lineString:find("x")+1, #lineString)
				a = tonumber(a)
				b = tonumber(b)
				--print(a,b)
				local answerIndex = #data[currentLevel].A + 1
				data[currentLevel].A[answerIndex] = {}
				data[currentLevel].A[answerIndex].result = a*b -- {a, b}
				data[currentLevel].A[answerIndex].width = a -- {a, b}
				data[currentLevel].A[answerIndex].height = b -- {a, b}

			elseif firstChar == "." or firstChar == "1" then
				-- problem
				data[currentLevel].Q[#data[currentLevel].Q + 1] = lineString
			end

			lineString = file:read("*l")
		until lineString == nil

		io.close( file )	-- close the file after using it

		local function printTable(tbl) 
			for i,v in ipairs(tbl) do
				print(i, v)
			end
		end
		--[[
		print("printing Data")

		for i=1, #data do 
			print(i)
			printTable(data[i].Q)
			printTable(data[i].A)
		--	for j=1, #data[i].A do 
		--		printTable(data[i].A[j])
		--	end
		end
		--]]

		function Count_Substring( s1, s2 )
		 local magic =  "[%^%$%(%)%%%.%[%]%*%+%-%?]"
		 local percent = function(s)return "%"..s end
		    return select( 2, s1:gsub( s2:gsub(magic,percent), "" ) )
		end

		for i=1,#data do
			local numberOfBlock = 0
			for j=1,#data[i].Q do
				numberOfBlock = numberOfBlock + Count_Substring(data[i].Q[j], "1")
			end

			local sumOfAnswer = 0;
			for j=1, #data[i].A do
				sumOfAnswer = sumOfAnswer + data[i].A[j].result
			end
			if numberOfBlock ~= sumOfAnswer then
				print("Problem is Wrong :  " .. i)
			end
		end

		return data
	else
		return nil;
	end

end
