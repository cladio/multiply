--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:1914df1ff56dd36b3e01a014b8348aff:c029626b1cd4fb2535366e8f3fbc0789:cdc667a1a7e32bb3e99d2d3a65e23f67$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- monster_11@2x
            x=2,
            y=2,
            width=246,
            height=761,

            sourceX = 127,
            sourceY = 215,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_12@2x
            x=250,
            y=2,
            width=246,
            height=761,

            sourceX = 127,
            sourceY = 215,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_01@2x
            x=498,
            y=2,
            width=246,
            height=761,

            sourceX = 127,
            sourceY = 215,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_10@2x
            x=746,
            y=2,
            width=259,
            height=762,

            sourceX = 112,
            sourceY = 152,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_08@2x
            x=1007,
            y=2,
            width=262,
            height=878,

            sourceX = 119,
            sourceY = 60,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_09@2x
            x=1271,
            y=2,
            width=274,
            height=874,

            sourceX = 114,
            sourceY = 62,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_02@2x
            x=1547,
            y=2,
            width=300,
            height=766,

            sourceX = 100,
            sourceY = 213,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_04@2x
            x=2,
            y=882,
            width=366,
            height=784,

            sourceX = 67,
            sourceY = 204,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_03@2x
            x=370,
            y=882,
            width=368,
            height=778,

            sourceX = 51,
            sourceY = 207,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_06@2x
            x=740,
            y=882,
            width=406,
            height=799,

            sourceX = 58,
            sourceY = 201,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_05@2x
            x=1148,
            y=882,
            width=424,
            height=771,

            sourceX = 38,
            sourceY = 211,
            sourceWidth = 500,
            sourceHeight = 1000
        },
        {
            -- monster_07@2x
            x=1574,
            y=882,
            width=436,
            height=926,

            sourceX = 32,
            sourceY = 37,
            sourceWidth = 500,
            sourceHeight = 1000
        },
    },
    
    sheetContentWidth = 2048,
    sheetContentHeight = 2048
}

SheetInfo.frameIndex =
{

    ["monster_11@2x"] = 1,
    ["monster_12@2x"] = 2,
    ["monster_01@2x"] = 3,
    ["monster_10@2x"] = 4,
    ["monster_08@2x"] = 5,
    ["monster_09@2x"] = 6,
    ["monster_02@2x"] = 7,
    ["monster_04@2x"] = 8,
    ["monster_03@2x"] = 9,
    ["monster_06@2x"] = 10,
    ["monster_05@2x"] = 11,
    ["monster_07@2x"] = 12,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
