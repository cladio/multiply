local storyboard = require( "storyboard" )
local block = require("block")
local answerCircle = require("answerCircle")
local widget = require("widget")


local printEmpty = function()	return; end
local DEBUG = printEmpty; 



local scene = storyboard.newScene()

----------------------------------------------------------------------------------
-- 
--      NOTE:
--      
--      Code outside of listener functions (below) will only be executed once,
--      unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

local G_levelText;
local G_gameBlocks;
local G_currentLevel
local G_levelData;
local G_equation

local G_answerNumber;
local G_colorTable = { 
	{238, 115, 97},
	{245, 155, 50}, 
	{250, 230, 50},
	{192, 212, 35}, 
	{140, 186, 126},
	{142, 177, 215},
	{157, 140, 196},
	{205, 131, 172},
	{238, 115, 97}
}
local G_defaultBaloonColor = {100, 100, 100}
local G_playStep = 0;


local function displayGoodJob()
	local obj = display.newGroup();

	obj.rect = display.newRect(obj, screenW*0.3, screenH * 0.4, screenW * 0.4, screenH * 0.2)
	obj.rect:setFillColor(180, 180, 180)

	obj.text = display.newText(obj, "Great!", 0, 0 , "Futura-Medium", 50);
	obj.text:setTextColor(255)
	obj.text:setReferencePoint(display.centerReferencePoint)
	obj.text.x = obj.rect.x
	obj.text.y = obj.rect.y

	obj.x = obj.x + screenW

	transition.to( obj, { time=500, x = 0, y = 0, onComplete = 
						function ()
							transition.to( obj, { time=500, delay = 1000, x = - screenW , y = 0, onComplete = function () 
												display.remove(obj)
											end
											} )
						end
						 } )

end



local function makeRandomGame(width, height, numberOfAnswers)
	
	local tbl = {};
	for y=1, height do
		tbl[y] = {}
		for x=1, width do
			tbl[y][x] = 1
		end
	end

	local function groupBlocks(tbl)
		local blockGroup = {}
		for i=1, #tbl do
			for j=1, #tbl[i] do
				local blockNumber = tbl[i][j]

				-- 새로 나온 숫자 블럭이면 table 생성 --
				if blockGroup[blockNumber] == nil then
					blockGroup[blockNumber] = {};
					blockGroup[blockNumber].startX = j;
					blockGroup[blockNumber].startY = i;
					blockGroup[blockNumber].endX = j;
					blockGroup[blockNumber].endY = i;
				else
					blockGroup[blockNumber].endX = j;
					blockGroup[blockNumber].endY = i;
				end

			end				
		end

		for i=1, #blockGroup do
			blockGroup[i].width = blockGroup[i].endX - blockGroup[i].startX + 1;
			blockGroup[i].height = blockGroup[i].endY - blockGroup[i].startY + 1;
			print(i, blockGroup[i].startX, blockGroup[i].startY, blockGroup[i].endX, blockGroup[i].endY, blockGroup[i].width, blockGroup[i].height);
		end
		

		return blockGroup;

	end

	local function printBlocks()
		-- for checking
		for i=1, #tbl do
			local str= ""
			for j=1, #tbl[i] do
				str = str .. tbl[i][j]
			end
			print(str);
		end
	end

	local function cutHorizontal(blockGroup, groupNumber, cut)
		print("Cutting Horizontal, Group/Cut : ", groupNumber , cut)
		for y=blockGroup[groupNumber].startY + cut -1, blockGroup[groupNumber].endY do
			for x=blockGroup[groupNumber].startX, blockGroup[groupNumber].endX do
				tbl[y][x] = #blockGroup + 1;
			end
		end
	end

	local function cutVertical(blockGroup, groupNumber, cut)
		print("Cutting Vertical, Group/Cut : ", groupNumber , cut)
		for y=blockGroup[groupNumber].startY, blockGroup[groupNumber].endY do
			for x=blockGroup[groupNumber].startX+ cut -1, blockGroup[groupNumber].endX do
				tbl[y][x] = #blockGroup + 1;
			end
		end
	end

	for i=1, numberOfAnswers -1 do
		local blockGroup = groupBlocks(tbl);
		while true do

			local rand = math.random(1, #blockGroup)
			if blockGroup[rand].width == 10 then
				local cut = math.random(2, blockGroup[rand].width)
				cutVertical(blockGroup, rand, cut);
				break;
			elseif blockGroup[rand].height == 10 then
				local cut = math.random(2, blockGroup[rand].height)
				cutHorizontal(blockGroup, rand, cut);
				break;
			elseif blockGroup[rand].width >= 3 and blockGroup[rand].height >= 3 then				
				if rand%2 == 1 then
					local cut = math.random(2, blockGroup[rand].height)
					cutHorizontal(blockGroup, rand, cut)
				else
					local cut = math.random(2, blockGroup[rand].width)
					cutVertical(blockGroup, rand, cut)
				end
				break;
			elseif blockGroup[rand].width < 3 and blockGroup[rand].height >= 3 then
				local cut = math.random(2, blockGroup[rand].height)
				cutHorizontal(blockGroup, rand, cut)
				break;
			elseif blockGroup[rand].width >= 3 and blockGroup[rand].height < 3 then
				local cut = math.random(2, blockGroup[rand].width)
				cutVertical(blockGroup, rand, cut)
				break;
			end
		end

		printBlocks();

	end

	-- genrate Answers
	local blockGroup = groupBlocks(tbl);
	local answerTable = {};

	for i=1, #blockGroup do
		answerTable[i] = blockGroup[i].width * blockGroup[i].height;
	end

	return tbl, answerTable;
	

end



local function createGameBlock(levelData)

	local startX, startY, currentX, currentY = 0, 0, 0, 0;

	local blocks = {};
	local answers = {};
	local result;


	local obj = display.newGroup();

	obj.fillSelectedBlocks = function () 

		local smallX = (startX > currentX) and currentX or startX
		local largeX = (startX > currentX) and startX or currentX
		local smallY = (startY > currentY) and currentY or startY
		local largeY = (startY > currentY) and startY or currentY

		
		for x=1,#blocks do
			for y=1,#blocks[x] do
				if blocks[x][y].confirmNumber ~= 0 and x>=smallX and x<= largeX and y >= smallY and y <= largeY then
					return;
				end
			end
		end

		result = (largeX-smallX+1)*(largeY-smallY+1)
		G_equation = tostring(largeX-smallX+1) .. " X " .. tostring(largeY-smallY+1) .. 
												" = " .. tostring(result)

		local matchAnswer = false;

		-- baloon and answer highlighting --
		for k=1, #answers do
			if answers[k].number == result and answers[k].solved == false and matchAnswer == false then
				matchAnswer = k
				answers[k].setExpandedText(G_equation)

			elseif answers[k].number ~= result and answers[k].solved == false then
				answers[k].setText(answers[k].number)
			end
		end

		for i=1,#blocks do
			for j=1,#blocks[i] do

				if i>= smallX and i<= largeX and j >= smallY and j <= largeY then
					if matchAnswer then
						blocks[i][j].setColor(G_colorTable[matchAnswer])
						blocks[i][j].select = true;
					else
						blocks[i][j].setColor();
						blocks[i][j].select = false;
					end
				elseif blocks[i][j].confirmNumber == 0 then
					blocks[i][j].reset();
					blocks[i][j].select = false;
				end

				if i == currentX and j == currentY then
					
					blocks[i][j].setBaloonText(G_equation)
				else
					blocks[i][j].removeBaloon();
				end
			end
		end

	end

	obj.callBack = function(x, y, phase)

		if phase == "began" then
			startX, startY = x, y;
			currentX, currentY = x, y;
			obj.fillSelectedBlocks()
		elseif phase == "moved" then
			if startX == 0 then
				startX, startY = x, y;
			end
			currentX, currentY = x, y;
			obj.fillSelectedBlocks();
		elseif phase == "ended" then
			print("ended")
			for i=1,#blocks do
				for j=1,#blocks[i] do
					if blocks[i][j].select == true then
						blocks[i][j].confirmNumber = G_playStep;
					else
						blocks[i][j].reset();
					end
					blocks[i][j].removeBaloon();
				end
			end
			startX, startY = 0, 0;

			local unsolved = false;
			for i=1, #answers do

				if answers[i].number == result and answers[i].solved == false then
					answers[i].solved = true;
					G_playStep = G_playStep + 1;
					break;
				end
			end

			for i=1, #answers do
				if answers[i].solved == false then
					unsolved = true;
				end

			end

			-- if level complete
			if unsolved == false then

				displayGoodJob();
				timer.performWithDelay(2000, function () 
							G_gameBlocks:removeSelf();
							G_gameBlocks = createGameBlock(10, 10, 6)
							 end)
			end

		end

	end


	local blockSize = 60;

	local function maxTable(tbl)
		local max = 0;
		for i=1, #tbl do
			if tbl[i]>max then
				max = tbl[i]
			end
		end
		return max;
	end

	local offsetX = screenW * 0.62 - maxTable(levelData.Q)/2*blockSize
	local offsetY = screenH * 0.5 - #levelData[G_currentLevel].Q/2*blockSize

	local blockNumber = 1;
	for y=1, #levelData.Q do
		for x=1, levelData.Q[y] do
			if y == 1 then
				blocks[x] = {}
			end

			blocks[x][y] = block.new(blockNumber, x, y, obj.callBack);
			blocks[x][y].isVisible = false;
			blocks[x][y].x = offsetX + blockSize*(x-1)
			blocks[x][y].y = offsetY + blockSize*(y-1)
			blocks[x][y].text.isVisible = false;
			blocks[x][y].select = false;
			blocks[x][y].confirmNumber = 0;
			G_playStep = 1;

			obj:insert(blocks[x][y]);

			blockNumber = blockNumber + 1;
			timer.performWithDelay(blockNumber*10, function () blocks[x][y].isVisible = true;
										end )
		end
	end

	-- display Answers

	for i=1, #levelData.A do
		answers[i] = answerCircle.new(levelData.A[i], G_colorTable[i]);
		answers[i].x = screenW * 0.19
		answers[i].y = i * 100
		answers[i].number = levelData.A [i]	
		answers[i].solved = false;	
		obj:insert(answers[i])
	end;

	startX, startY, currentX, currentY = 0, 0, 0, 0

	obj.remove = function ()
		display.remove(obj)
	end

	obj.reset = function ()
		for i=1, 10 do
			for j=1, 10 do
				blocks[i][j].reset();
				blocks[i][j].select = false;
				blocks[i][j].confirmNumber = 0;
				G_playStep = 1;
			end
		end

		for i=1, #answerTable do
			answers[i].solved = false;	
			answers[i].setText(levelData.A[i])
		end;
	end

	return obj;

end




-- Called when the scene's view does not exist:
function scene:createScene( event )
        local group = self.view
		
        -----------------------------------------------------------------------------

        --      CREATE display objects and add them to 'group' here.
        --      Example use-case: Restore 'group' from previously saved state.

        -----------------------------------------------------------------------------
		
		local bg = display.newRect(group, 0, 0, screenW, screenH );
    	bg:setFillColor(255, 255, 255) 

    	function bg:touch(event)
    		if event.phase == "ended" then
    			G_gameBlocks.callBack(0,0,"ended");
    		end

		end
		
		bg:addEventListener("touch", bg)

		G_levelText = display.newText(group, "", 50, 40, native.systemFont, 60)
		G_levelText:setTextColor(0);


		local line = display.newLine(group, 130, 0, 130, screenH)
		line:setColor(128)
		line.width = 5;

		local function buttonEvent( event )
		    local phase = event.phase 

		    if "ended" == phase then
		    	if event.target.id == "back" then
					storyboard.gotoScene( "sceneGameSelect" , options)
				elseif event.target.id == "refresh" then
					G_gameBlocks.reset();
				end
		    end
		end

		-- Create the button
		local backButton = widget.newButton
		{
		    left = 40,
		    top = 600,
		    width = 50,
		    height = 50,
		    defaultFile = "images/backButton.png",
		    overFile = "images/backButton.png",
		    id = "back",
		    onEvent = buttonEvent,
		}
		group:insert(backButton)

		local refreshButton = widget.newButton
		{
		    left = 40,
		    top = 680,
		    width = 50,
		    height = 50,
		    defaultFile = "images/refreshButton.png",
		    overFile = "images/refreshButton.png",
		    id = "refresh",
		    onEvent = buttonEvent,
		}
		group:insert(refreshButton)


		printMem();
end	


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. start timers, load audio, start listeners, etc.)

        -----------------------------------------------------------------------------
--        makeRandomGame(10, 10, 3)
	
		local levelData = event.params.levelData;
        G_levelData = levelData;

        G_currentLevel = event.params.level;
        if G_currentLevel > #levelData then
        	G_currentLevel = #levelData;
        end

        if event.params.random == true then
			local randomLevelData = makeRandomGame(10, 10, 6);
			G_gameBlocks = createGameBlock(randomLevelData)
		else
	        G_levelText.text = G_currentLevel;
	        G_gameBlocks = createGameBlock(levelData[G_currentLevel])
	    end
        group:insert(G_gameBlocks)

end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)

        -----------------------------------------------------------------------------
        group:remove(G_gameBlocks);
        G_gameBlocks:removeSelf();

end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. remove listeners, widgets, save state, etc.)

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end



---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene