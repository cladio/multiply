local storyboard = require "storyboard"
local widget = require "widget"

-- screen size setting
screenW = display.contentWidth
screenH = display.contentHeight
screenCenterX = screenW/2
screenCenterY = screenH/2

display.setStatusBar( display.HiddenStatusBar )

-- monitoring memory section (for debug purpose); 
function printMem()
	collectgarbage();
   	local textMem = system.getInfo( "textureMemoryUsed" ) / 1000000;
	print( "MemUsage: " .. collectgarbage("count") .. "  TexMem:   " .. textMem );
end

function color255to1(c1, c2, c3, c4)
	if c4 then 
		return c1/255, c2/255, c3/255, c4/255
	else 
		return c1/255, c2/255, c3/255
	end
end



math.randomseed( os.time() )

widget.setTheme( "widget_theme_ios7" )

G_colorTable_255 = { 
	{238, 115, 97},
	{245, 155, 50}, 
	{250, 230, 50},
	{192, 212, 35}, 
	{140, 186, 126},
	{142, 177, 215},
	{157, 140, 196},
	{205, 131, 172},
	{238, 115, 97},
	{245, 155, 50}, 
	{250, 230, 50},
	{192, 212, 35}, 
	{140, 186, 126},
	{142, 177, 215},
	{157, 140, 196},
	{205, 131, 172},
	{238, 115, 97},
	{245, 155, 50}, 
	{250, 230, 50},
	{192, 212, 35}, 
	{140, 186, 126},
	{142, 177, 215},
	{157, 140, 196},
	{205, 131, 172},
}

G_colorTable = {}
for i=1, #G_colorTable_255 do 
	G_colorTable[i] = {};
	G_colorTable[i][1] = G_colorTable_255[i][1]/255
	G_colorTable[i][2] = G_colorTable_255[i][2]/255
	G_colorTable[i][2] = G_colorTable_255[i][2]/255
end

local splash = display.newImage("images/splash.png")
splash.x = screenW*0.5
splash.y = screenH*0.5

timer.performWithDelay(300, function () display.remove(splash)
									storyboard.gotoScene( "sceneGameSelect" ) end)


